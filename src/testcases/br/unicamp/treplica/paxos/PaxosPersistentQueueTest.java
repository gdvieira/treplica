/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos;

import junit.framework.TestCase;

import static org.junit.Assert.assertThrows;

import java.util.LinkedList;

import br.unicamp.treplica.PersistentQueue;
import br.unicamp.treplica.StateManager;
import br.unicamp.treplica.TestStateManager;
import br.unicamp.treplica.common.Marshall;
import br.unicamp.treplica.common.TestChangeLog;
import br.unicamp.treplica.common.TestChangeLogClient;
import br.unicamp.treplica.common.TestTransport;
import br.unicamp.treplica.paxos.ledger.Ledger;

public class PaxosPersistentQueueTest extends TestCase {

    @Override
    protected void setUp() {
    }
    
    @Override
    protected void tearDown() {
    }

    /*
     * Tests if the global delivery state only moves if all processes are
     * accounted for.
     */
    public void testGlobalDeliveryState1 () {
        GlobalDeliveryState state = new GlobalDeliveryState(3);
        assertEquals(Ledger.FIRST_COUNTER, state.getGlobalNextToDeliver());
        state.updateGlobalDeliveries(0, 5);
        state.updateGlobalDeliveries(1, 5);
        assertEquals(Ledger.FIRST_COUNTER, state.getGlobalNextToDeliver());
        state.updateGlobalDeliveries(2, 5);
        assertEquals(5, state.getGlobalNextToDeliver());
    }

    /*
     * Tests if the global delivery state always reflects the smaller
     * nextToDeliver.
     */
    public void testGlobalDeliveryState2 () {
        GlobalDeliveryState state = new GlobalDeliveryState(3);
        state.updateGlobalDeliveries(0, 5);
        state.updateGlobalDeliveries(1, 5);
        state.updateGlobalDeliveries(2, 5);
        assertEquals(5, state.getGlobalNextToDeliver());
        state.updateGlobalDeliveries(0, 15);
        assertEquals(5, state.getGlobalNextToDeliver());
        state.updateGlobalDeliveries(1, 25);
        assertEquals(5, state.getGlobalNextToDeliver());
        state.updateGlobalDeliveries(0, 26);
        assertEquals(5, state.getGlobalNextToDeliver());
        state.updateGlobalDeliveries(2, 12);
        assertEquals(12, state.getGlobalNextToDeliver());
    }

    public void testClassic() throws Exception {
        TestTransport t = new TestTransport();
        PersistentQueue paxos = 
            new PaxosPersistentQueue(10, 1, false, t, new TestChangeLog());
        paxos.bind(new TestStateManager());
        Thread.sleep(20);
        paxos.enqueue("hello1");
        paxos.enqueue("hello2");
        assertEquals("hello1", paxos.dequeue());
        assertEquals("hello2", paxos.dequeue());
        paxos.enqueue("hello3");
        paxos.enqueue("hello4");
        assertEquals("hello3", paxos.dequeue());
        assertEquals("hello4", paxos.dequeue());
    }

    public void testFast() throws Exception {
        TestTransport t = new TestTransport();
        PersistentQueue paxos = 
            new PaxosPersistentQueue(10, 1, true, t, new TestChangeLog());
        paxos.bind(new TestStateManager());
        Thread.sleep(20);
        paxos.enqueue("hello1");
        paxos.enqueue("hello2");
        assertEquals("hello1", paxos.dequeue());
        assertEquals("hello2", paxos.dequeue());
        paxos.enqueue("hello3");
        paxos.enqueue("hello4");
        assertEquals("hello3", paxos.dequeue());
        assertEquals("hello4", paxos.dequeue());
    }
    
    public void testNull() throws Exception {
        TestTransport t = new TestTransport();
        PersistentQueue paxos = 
            new PaxosPersistentQueue(10, 1, true, t, new TestChangeLog());
        paxos.bind(new TestStateManager());
        Thread.sleep(20);
        paxos.enqueue(null);
        assertNull(paxos.dequeue());
    }
    
    public void testCheckpoint() throws Exception {
        TestTransport t = new TestTransport();
        TestChangeLog l = new TestChangeLog();
        PaxosPersistentQueue paxos = 
            new PaxosPersistentQueue(20, 1, true, t, l);
        paxos.bind(new TestStateManager());
        Thread.sleep(40);
        paxos.enqueue("hello1");
        Thread.sleep(40);
        paxos.enqueue("hello2");
        assertEquals("hello1", paxos.dequeue());
        Thread.sleep(40);
        paxos.checkpoint();
        paxos.enqueue("hello3");
        Thread.sleep(40);
        
        Secretary secretary = new Secretary(l.copy(), null);
        assertEquals(2, secretary.bind(10, new TestStateManager()));
        Ledger ledger = secretary.getLedger();
        assertEquals(Marshall.unmarshall(ledger.read(0).getMessages()[0]), 
                "hello1");
        assertEquals(Marshall.unmarshall(ledger.read(1).getMessages()[0]), 
                "hello2");
        assertEquals(Marshall.unmarshall(ledger.read(2).getMessages()[0]), 
                "hello3");
        assertNull(ledger.read(3));
        TestChangeLogClient client = new TestChangeLogClient();
        l.copy().open(client);
        assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint)
                client.lastCheckpoint).application);
        assertEquals(2, ((PaxosCheckpoint)
                client.lastCheckpoint).nextToDeliver);
        LinkedList<byte[]> queue = ((PaxosCheckpoint)
                client.lastCheckpoint).deliveryQueue;
        assertEquals(1, queue.size());
        assertEquals("hello2", Marshall.unmarshall(queue.removeFirst()));
        ledger = ((PaxosCheckpoint) client.lastCheckpoint).ledger;
        assertEquals(Marshall.unmarshall(ledger.read(0).getMessages()[0]), 
                "hello1");
        assertEquals(Marshall.unmarshall(ledger.read(1).getMessages()[0]), 
                "hello2");
        assertNull(ledger.read(2));
        assertEquals(2, client.changes.size());
    }
    
    public void testCheckpointAndGarbageCollect() throws Exception {
        TestTransport t = new TestTransport();
        TestChangeLog l = new TestChangeLog();
        PaxosPersistentQueue paxos =
            new PaxosPersistentQueue(5, 1, false, t, l);
        StateManager state = new TestStateManager();
        paxos.bind(state);
        paxos.enqueue("hello1");
        paxos.enqueue("hello2");
        assertEquals("hello1", paxos.dequeue());
        state.setState("hello1");
        PaxosMessage m = PaxosMessage.createStatus(0, 2);
        t.sendMessage(m);
        Thread.sleep(10);
        paxos.checkpoint();

        Secretary secretary = new Secretary(l.copy(), null);
        assertEquals(2, secretary.bind(10, new TestStateManager()));
        final Ledger ledger = secretary.getLedger();
        assertThrows(IndexOutOfBoundsException.class, () -> {
            ledger.read(0);
        });
        assertThrows(IndexOutOfBoundsException.class, () -> {
            ledger.read(1);
        });
        assertNull(ledger.read(2));
        TestChangeLogClient client = new TestChangeLogClient();
        l.copy().open(client);
        assertEquals("hello1", ((PaxosCheckpoint)
                client.lastCheckpoint).application);
        assertEquals(2, ((PaxosCheckpoint)
                client.lastCheckpoint).nextToDeliver);
        LinkedList<byte[]> queue = ((PaxosCheckpoint)
                client.lastCheckpoint).deliveryQueue;
        assertEquals(1, queue.size());
        assertEquals("hello2", Marshall.unmarshall(queue.removeFirst()));
        final Ledger ledger2 = ((PaxosCheckpoint) client.lastCheckpoint).ledger;
        assertThrows(IndexOutOfBoundsException.class, () -> {
            ledger2.read(0);
        });
        assertThrows(IndexOutOfBoundsException.class, () -> {
            ledger2.read(1);
        });
        assertNull(ledger.read(2));
    }

    public void testCheckpointAndGarbageCollectErasedGlobalPoint()
            throws Exception {
        TestTransport t = new TestTransport();
        TestChangeLog l = new TestChangeLog();
        Secretary secretary = new Secretary(l, null);
        secretary.bind(10, new TestStateManager());
        Ledger ledger = secretary.getLedger();
        ledger.write(0, new Proposal(0,
                new byte[][] { Marshall.marshall("hello1") }));
        ledger.write(1, new Proposal(1,
                new byte[][] { Marshall.marshall("hello2") }));
        ledger.erase(2);
        secretary.synchronousFlush();
        secretary.takeCheckpoint("checkpoint", 2);
        PersistentQueue paxos =
            new PaxosPersistentQueue(5, 1, true, t, l);
        StateManager state = new TestStateManager();
        paxos.bind(state);
        Thread.sleep(10);
        paxos.checkpoint();

        secretary = new Secretary(l.copy(), null);
        assertEquals(2, secretary.bind(10, new TestStateManager()));
        Ledger ledger2 = secretary.getLedger();
        assertThrows(IndexOutOfBoundsException.class, () -> {
            ledger2.read(0);
        });
        assertThrows(IndexOutOfBoundsException.class, () -> {
            ledger2.read(1);
        });
        assertNull(ledger.read(2));
        TestChangeLogClient client = new TestChangeLogClient();
        l.copy().open(client);
        assertEquals("checkpoint", ((PaxosCheckpoint)
                client.lastCheckpoint).application);
        assertEquals(2, ((PaxosCheckpoint)
                client.lastCheckpoint).nextToDeliver);
        LinkedList<byte[]> queue = ((PaxosCheckpoint)
                client.lastCheckpoint).deliveryQueue;
        assertEquals(0, queue.size());
    }

    public void testRecoveryNoCheckpoint() throws Exception {
        TestTransport t = new TestTransport();
        TestChangeLog l = new TestChangeLog();
        Secretary secretary = new Secretary(l, null);
        secretary.bind(10, new TestStateManager());
        Ledger ledger = secretary.getLedger();
        ledger.write(0, new Proposal(0, 
                new byte[][] { Marshall.marshall("hello1") }));
        ledger.write(1, new Proposal(1, 
                new byte[][] { Marshall.marshall("hello2") }));
        secretary.synchronousFlush();
        PersistentQueue paxos = 
            new PaxosPersistentQueue(10, 1, true, t, l);
        paxos.bind(new TestStateManager());
        Thread.sleep(20);
        paxos.enqueue("hello3");
        assertEquals("hello1", paxos.dequeue());
        assertEquals("hello2", paxos.dequeue());
        paxos.enqueue("hello4");
        assertEquals("hello3", paxos.dequeue());
        assertEquals("hello4", paxos.dequeue());
    }
    
    public void testRecoveryCheckpoint1() throws Exception {
        TestTransport t = new TestTransport();
        TestChangeLog l = new TestChangeLog();
        Secretary secretary = new Secretary(l, null);
        secretary.bind(10, new TestStateManager());
        Ledger ledger = secretary.getLedger();
        ledger.write(0, new Proposal(0, 
                new byte[][] { Marshall.marshall("hello1") }));
        ledger.write(1, new Proposal(1, 
                new byte[][] { Marshall.marshall("hello2") }));
        secretary.synchronousFlush();
        secretary.takeCheckpoint("checkpoint", 1);
        PersistentQueue paxos = 
            new PaxosPersistentQueue(10, 1, true, t, l);
        StateManager state = new TestStateManager();
        paxos.bind(state);
        Thread.sleep(20);
        assertEquals("checkpoint", state.getState());
        assertEquals("hello2", paxos.dequeue());
    }

    public void testRecoveryCheckpoint2() throws Exception {
        TestTransport t = new TestTransport();
        TestChangeLog l = new TestChangeLog();
        Secretary secretary = new Secretary(l, null);
        secretary.bind(10, new TestStateManager());
        Ledger ledger = secretary.getLedger();
        ledger.write(0, new Proposal(0, 
                new byte[][] { Marshall.marshall("hello1") }));
        ledger.write(1, new Proposal(1, 
                new byte[][] { Marshall.marshall("hello2") }));
        secretary.synchronousFlush();
        secretary.takeCheckpoint("checkpoint", 1);
        ledger.write(2, new Proposal(1, 
                new byte[][] { Marshall.marshall("hello3") }));
        secretary.synchronousFlush();
        PersistentQueue paxos = 
            new PaxosPersistentQueue(10, 1, true, t, l);
        StateManager state = new TestStateManager();
        paxos.bind(state);
        Thread.sleep(20);
        assertEquals("checkpoint", state.getState());
        assertEquals("hello2", paxos.dequeue());
        assertEquals("hello3", paxos.dequeue());
    }

    /*
     * Tests if the delivery queue is stored as part of a checkpoint.
     */
    public void testRecoveryCheckpoint3() throws Exception {
        TestTransport t = new TestTransport();
        TestChangeLog l = new TestChangeLog();
        Secretary secretary = new Secretary(l, null);
        secretary.bind(10, new TestStateManager());
        Ledger ledger = secretary.getLedger();
        ledger.write(0, new Proposal(0,
                new byte[][] { Marshall.marshall("hello1"),
                               Marshall.marshall("hello2")}));
        ledger.write(1, new Proposal(1,
                new byte[][] { Marshall.marshall("hello3") }));
        ledger.write(2, new Proposal(2,
                new byte[][] { Marshall.marshall("hello4") }));
        secretary.synchronousFlush();
        PersistentQueue paxos =
            new PaxosPersistentQueue(10, 1, false, t, l);
        StateManager state = new TestStateManager();
        paxos.bind(state);
        Thread.sleep(20);
        assertEquals(TestStateManager.INITIAL, state.getState());
        assertEquals("hello1", paxos.dequeue());
        state.setState("checkpoint");
        paxos.checkpoint();

        paxos = new PaxosPersistentQueue(10, 1, false, new TestTransport(),
                l.copy());
        paxos.bind(state);
        Thread.sleep(20);
        assertEquals("checkpoint", state.getState());
        assertEquals("hello2", paxos.dequeue());
        assertEquals("hello3", paxos.dequeue());
        assertEquals("hello4", paxos.dequeue());
    }

}
