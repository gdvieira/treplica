/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos;

import br.unicamp.treplica.TestStateManager;
import br.unicamp.treplica.TreplicaException;
import br.unicamp.treplica.common.Message;
import br.unicamp.treplica.common.TestChangeLog;
import br.unicamp.treplica.common.TestTransport;
import junit.framework.TestCase;

public class PaxosElectionTest extends TestCase {

    private TestTransport t;
    private Secretary s;
    private Client ec;
    private Election e;
    
    @Override
    protected void setUp() throws TreplicaException {
        t = new TestTransport();
        s = new Secretary(new TestChangeLog(), t);
        s.bind(2, new TestStateManager());
        ec = new Client();
        e = new Election(s, 10, ec);
    }

    /*
     * Test if after a tick is processed the next one will be queued at the
     * right time. First as a follower, then as a leader.
     */
    public void testProcessTick()
            throws InterruptedException, TreplicaException {
        int delta = 10;
        long nextTick = e.processTick();
        assertEquals(nextTick, System.currentTimeMillis() + delta);
        Thread.sleep(15);
        nextTick = e.processTick();
        assertEquals(nextTick, System.currentTimeMillis() + (delta / 2));
    }

    public void testSingleLeader()
            throws InterruptedException, TreplicaException {
        assertFalse(e.knowsLeader());
        Thread.sleep(15);
        e.processTick();
        assertTrue(e.knowsLeader());
        assertEquals(s.getPaxosId(), e.getLeader());
        assertEquals(1, ec.counter);
    }

    public void testElectionPing()
            throws InterruptedException, TreplicaException {
        Thread.sleep(15);
        e.processTick();
        Thread.sleep(15);
        e.processTick();
        s.synchronousFlush();
        Message m = t.popMemory();
        assertEquals(e.getLeader(), 
                     ((PaxosMessage) m.getPayload()).getSender());
        assertEquals(PaxosMessage.LEADER, 
                     ((PaxosMessage) m.getPayload()).getType());
        assertEquals(1, ((PaxosMessage) m.getPayload()).getCounter());
        assertEquals(1, ec.counter);
    }
    
    public void testNewLeader1() throws TreplicaException {
        int leader = 1;
        e.processMessage(PaxosMessage.createLeader(leader, 1));
        assertTrue(e.knowsLeader());
        assertEquals(leader, e.getLeader());
        e.processTick();
        assertTrue(e.knowsLeader());
        assertEquals(leader, e.getLeader());
        e.processMessage(PaxosMessage.createLeader(leader, 2));
        assertTrue(e.knowsLeader());
        assertEquals(leader, e.getLeader());
        assertEquals(1, ec.counter);
    }

    public void testNewLeader2() throws TreplicaException {
        int leader = 3;
        e.processMessage(PaxosMessage.createLeader(leader, 0));
        assertTrue(e.knowsLeader());
        assertEquals(leader, e.getLeader());
        e.processTick();
        assertTrue(e.knowsLeader());
        assertEquals(leader, e.getLeader());
        e.processMessage(PaxosMessage.createLeader(leader, 1));
        assertTrue(e.knowsLeader());
        assertEquals(leader, e.getLeader());
        assertEquals(1, ec.counter);
    }

    public void testNewLeader3()
            throws InterruptedException, TreplicaException {
        int leader = 1;
        e.processMessage(PaxosMessage.createLeader(leader, 1));
        assertTrue(e.knowsLeader());
        assertEquals(leader, e.getLeader());
        Thread.sleep(15);
        e.processTick();
        s.synchronousFlush();
        assertNull(t.popMemory());
        assertTrue(e.knowsLeader());
        assertEquals(s.getPaxosId(), e.getLeader());
        e.processMessage(PaxosMessage.createLeader(leader, 2));
        assertTrue(e.knowsLeader());
        assertEquals(leader, e.getLeader());
        assertEquals(3, ec.counter);
    }

    public void testNewFollower1()
            throws InterruptedException, TreplicaException {
        int follower = 3;
        assertFalse(e.knowsLeader());
        Thread.sleep(15);
        e.processTick();
        assertTrue(e.knowsLeader());
        Thread.sleep(15);
        e.processTick();
        Thread.sleep(15);
        e.processTick();
        e.processMessage(PaxosMessage.createLeader(follower, 1));
        Thread.sleep(15);
        e.processTick();
        e.processMessage(PaxosMessage.createLeader(follower, 2));
        assertTrue(e.knowsLeader());
        assertEquals(s.getPaxosId(), e.getLeader());
        assertEquals(1, ec.counter);
    }

    public void testNewFollower2()
            throws InterruptedException, TreplicaException {
        int follower = 1;
        assertFalse(e.knowsLeader());
        Thread.sleep(15);
        e.processTick();
        assertTrue(e.knowsLeader());
        Thread.sleep(15);
        e.processTick();
        e.processMessage(PaxosMessage.createLeader(follower, 1));
        Thread.sleep(15);
        e.processTick();
        e.processMessage(PaxosMessage.createLeader(follower, 2));
        assertTrue(e.knowsLeader());
        assertEquals(s.getPaxosId(), e.getLeader());
        assertEquals(1, ec.counter);
    }

    public class Client implements ElectionClient {
        public int counter = 0;
        @Override
        public void leaderChange(int newLeader) {
            counter++;
        }
    }
}
