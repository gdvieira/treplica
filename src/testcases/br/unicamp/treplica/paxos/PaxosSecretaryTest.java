/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos;

import java.util.LinkedList;

import junit.framework.TestCase;
import br.unicamp.treplica.TestStateManager;
import br.unicamp.treplica.TreplicaException;
import br.unicamp.treplica.common.TestChangeLog;
import br.unicamp.treplica.common.TestChangeLogClient;
import br.unicamp.treplica.common.TestTransport;
import br.unicamp.treplica.common.TestTransportId;
import br.unicamp.treplica.paxos.ledger.GenerateNextClassicBallotNumber;
import br.unicamp.treplica.paxos.ledger.GenerateNextFastBallotNumber;
import br.unicamp.treplica.paxos.ledger.Ledger;
import br.unicamp.treplica.paxos.ledger.LedgerChange;
import br.unicamp.treplica.paxos.ledger.LoggingLedger;
import br.unicamp.treplica.paxos.ledger.SetInactiveLastTried;
import br.unicamp.treplica.paxos.ledger.SetInactiveNextBallot;

public class PaxosSecretaryTest extends TestCase {

    private TestTransport t;
    private TestChangeLog log;
    private Secretary s;

    @Override
    protected void setUp() throws Exception {
        t = new TestTransport();
        log = new TestChangeLog();
        s = new Secretary(log, t);
        s.bind(10, new TestStateManager());
    }

    public void testBind() throws TreplicaException {
        TestChangeLogClient client = new TestChangeLogClient();
        log.copy().open(client);
        assertEquals(10, ((PaxosCheckpoint) 
                client.lastCheckpoint).ledger.getPaxosId());
        assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint) 
                client.lastCheckpoint).application);
        assertEquals(0, ((PaxosCheckpoint) 
                client.lastCheckpoint).deliveryQueue.size());
        assertEquals(Ledger.FIRST_COUNTER, ((PaxosCheckpoint) 
                client.lastCheckpoint).nextToDeliver);
        assertEquals(0, client.changes.size());
    }
    
    public void testMessages() throws Exception {
        s.queueMessage("hello1");
        s.queueMessage("hello2", new TestTransportId(2));
        assertNull(t.popMemory());
        s.synchronousFlush();
        assertEquals("hello1", t.popMemory().getPayload());
        assertEquals("hello2", t.popMemory().getPayload());
        assertNull(t.popMemory());
        assertEquals(0, s.getDeliveryQueueSize());
    }

    public void testDeliveries() throws Exception {
        byte[] m1 = new byte[] {23};
        byte[] m2 = new byte[] {42};
        s.queueDelivery(m1);
        s.queueDelivery(m2);
        assertEquals(0, s.getDeliveryQueueSize());
        s.synchronousFlush();
        assertEquals(2, s.getDeliveryQueueSize());
        assertEquals(m1, s.getDelivery());
        assertEquals(m2, s.getDelivery());
        assertEquals(0, s.getDeliveryQueueSize());
        assertNull(t.popMemory());
    }
    
    public void testUnchangedLedger() throws Exception {
        byte[] m = new byte[] {23};
        s.queueMessage("hello1");
        s.queueDelivery(m);
        s.synchronousFlush();
        assertEquals("hello1", t.popMemory().getPayload());
        assertEquals(1, s.getDeliveryQueueSize());
        assertEquals(m, s.getDelivery());
        TestChangeLogClient client = new TestChangeLogClient();
        log.copy().open(client);
        assertEquals(10, ((PaxosCheckpoint) 
                client.lastCheckpoint).ledger.getPaxosId());
        assertEquals(TestStateManager.INITIAL, ((PaxosCheckpoint) 
                client.lastCheckpoint).application);
        assertEquals(0, ((PaxosCheckpoint) 
                client.lastCheckpoint).deliveryQueue.size());
        assertEquals(Ledger.FIRST_COUNTER, ((PaxosCheckpoint) 
                client.lastCheckpoint).nextToDeliver);
        assertEquals(0, client.changes.size());
    }
    
    public void testLedger1() throws Exception {
        Ledger ledger = s.getLedger();
        ledger.setInactiveLastTried(ledger.generateNextFastBallotNumber());
        
        TestChangeLogClient client = new TestChangeLogClient();
        log.copy().open(client);
        assertEquals(0, client.changes.size());
        
        s.synchronousFlush();
        client = new TestChangeLogClient();
        log.copy().open(client);
        assertEquals(1, client.changes.size());
        LedgerChange[] changes = (LedgerChange[]) client.changes.get(0);
        assertEquals(2, changes.length);
        assertTrue(changes[0] instanceof GenerateNextFastBallotNumber);
        assertTrue(changes[1] instanceof SetInactiveLastTried);
    }
    
    public void testLedger2() throws Exception {
        Ledger ledger = s.getLedger();
        ledger.setInactiveLastTried(ledger.generateNextFastBallotNumber());
        s.synchronousFlush();
        ledger.setInactiveNextBallot(ledger.generateNextClassicBallotNumber());
        
        s.synchronousFlush();
        TestChangeLogClient client = new TestChangeLogClient();
        log.copy().open(client);
        assertEquals(2, client.changes.size());
        LedgerChange[] changes = (LedgerChange[]) client.changes.get(0);
        assertEquals(2, changes.length);
        assertTrue(changes[0] instanceof GenerateNextFastBallotNumber);
        assertTrue(changes[1] instanceof SetInactiveLastTried);
        changes = (LedgerChange[]) client.changes.get(1);
        assertEquals(2, changes.length);
        assertTrue(changes[0] instanceof GenerateNextClassicBallotNumber);
        assertTrue(changes[1] instanceof SetInactiveNextBallot);
    }
    
    public void testCheckpoint() throws Exception {
        s.takeCheckpoint("checkpoint", 2);
        TestChangeLogClient client = new TestChangeLogClient();
        log.copy().open(client);
        assertEquals(10, ((PaxosCheckpoint) 
                client.lastCheckpoint).ledger.getPaxosId());
        assertEquals("checkpoint", ((PaxosCheckpoint) 
                client.lastCheckpoint).application);
        assertEquals(0, ((PaxosCheckpoint) 
                client.lastCheckpoint).deliveryQueue.size());
        assertEquals(2, ((PaxosCheckpoint) 
                client.lastCheckpoint).nextToDeliver);
        assertEquals(0, client.changes.size());
    }
    
    public void testCheckpointAndChanges1() throws Exception {
        Ledger ledger = s.getLedger();
        BallotNumber ballot = ledger.generateNextFastBallotNumber();
        ledger.setInactiveLastTried(ballot);
        s.synchronousFlush();
        s.takeCheckpoint("checkpoint", 2);
        TestChangeLogClient client = new TestChangeLogClient();
        log.copy().open(client);
        ledger = ((PaxosCheckpoint) client.lastCheckpoint).ledger;
        assertEquals(10, ledger.getPaxosId());
        assertEquals(ballot, ledger.getInactiveLastTried());
        assertEquals("checkpoint", ((PaxosCheckpoint) 
                client.lastCheckpoint).application);
        assertEquals(0, ((PaxosCheckpoint) 
                client.lastCheckpoint).deliveryQueue.size());
        assertEquals(2, ((PaxosCheckpoint) 
                client.lastCheckpoint).nextToDeliver);
        assertEquals(0, client.changes.size());
    }
    
    public void testCheckpointAndChanges2() throws Exception {
        s.takeCheckpoint("checkpoint", 2);
        Ledger ledger = s.getLedger();
        ledger.setInactiveLastTried(ledger.generateNextFastBallotNumber());
        s.synchronousFlush();
        TestChangeLogClient client = new TestChangeLogClient();
        log.copy().open(client);
        ledger = ((PaxosCheckpoint) client.lastCheckpoint).ledger;
        assertEquals(10, ledger.getPaxosId());
        assertNull(ledger.getInactiveLastTried());
        assertEquals("checkpoint", ((PaxosCheckpoint) 
                client.lastCheckpoint).application);
        assertEquals(0, ((PaxosCheckpoint) 
                client.lastCheckpoint).deliveryQueue.size());
        assertEquals(2, ((PaxosCheckpoint) 
                client.lastCheckpoint).nextToDeliver);
        assertEquals(1, client.changes.size());
        LedgerChange[] changes = (LedgerChange[]) client.changes.get(0);
        assertEquals(2, changes.length);
        assertTrue(changes[0] instanceof GenerateNextFastBallotNumber);
        assertTrue(changes[1] instanceof SetInactiveLastTried);
    }

    public void testCheckpointAndQueue1() throws Exception {
        byte[] m = new byte[] {23};
        s.queueDelivery(m);
        s.synchronousFlush();
        s.takeCheckpoint("checkpoint", 2);
        TestChangeLogClient client = new TestChangeLogClient();
        log.copy().open(client);
        assertEquals("checkpoint", ((PaxosCheckpoint) 
                client.lastCheckpoint).application);
        LinkedList<byte[]> queue = ((PaxosCheckpoint) 
                client.lastCheckpoint).deliveryQueue;
        assertEquals(1, queue.size());
        assertEquals(m[0], queue.removeFirst()[0]);
        assertNull(queue.peekFirst());
        assertEquals(2, ((PaxosCheckpoint) 
                client.lastCheckpoint).nextToDeliver);
        assertEquals(0, client.changes.size());
    }

    public void testCheckpointAndQueue2() throws Exception {
        s.takeCheckpoint("checkpoint", 2);
        byte[] m = new byte[] {23};
        s.queueDelivery(m);
        s.synchronousFlush();
        TestChangeLogClient client = new TestChangeLogClient();
        log.copy().open(client);
        assertEquals("checkpoint", ((PaxosCheckpoint) 
                client.lastCheckpoint).application);
        assertEquals(0, ((PaxosCheckpoint) 
                client.lastCheckpoint).deliveryQueue.size());
        assertEquals(2, ((PaxosCheckpoint) 
                client.lastCheckpoint).nextToDeliver);
        assertEquals(0, client.changes.size());
    }

    public void testRecoveryNoClientCheckpoint() throws Exception {
        LoggingLedger loggingLedger = new LoggingLedger(10);
        BallotNumber ballot = loggingLedger.generateNextFastBallotNumber();
        loggingLedger.setInactiveLastTried(ballot);
        TestChangeLog newLog = new TestChangeLog();
        newLog.open(new TestChangeLogClient());
        newLog.writeCheckpoint(
                new PaxosCheckpoint(loggingLedger, TestStateManager.INITIAL,
                        new LinkedList<>(), Ledger.FIRST_COUNTER));
        newLog.writeChange(loggingLedger.flushLog());
        loggingLedger.setInactiveNextBallot(ballot);
        newLog.writeChange(loggingLedger.flushLog());

        s = new Secretary(newLog.copy(), t);
        TestStateManager state = new TestStateManager();
        assertEquals(Ledger.FIRST_COUNTER, s.bind(20, state));
        assertEquals(TestStateManager.INITIAL, state.state);
        Ledger ledger = s.getLedger();
        assertEquals(10, ledger.getPaxosId());
        assertEquals(ballot, ledger.getInactiveLastTried());
        assertEquals(ballot, ledger.getInactiveNextBallot());
        assertEquals(0, s.getDeliveryQueueSize());
    }

    public void testRecoveryNoChanges() throws Exception {
        LoggingLedger loggingLedger = new LoggingLedger(10);
        BallotNumber ballot = loggingLedger.generateNextFastBallotNumber();
        loggingLedger.setInactiveLastTried(ballot);
        loggingLedger.setInactiveNextBallot(ballot);
        TestChangeLog newLog = new TestChangeLog();
        newLog.open(new TestChangeLogClient());
        newLog.writeCheckpoint(
                new PaxosCheckpoint(loggingLedger, TestStateManager.INITIAL,
                        new LinkedList<>(), Ledger.FIRST_COUNTER));
        newLog.writeCheckpoint(
                new PaxosCheckpoint(loggingLedger, "checkpoint", 
                        new LinkedList<>(), 2));

        s = new Secretary(newLog.copy(), t);
        TestStateManager state = new TestStateManager();
        assertEquals(2, s.bind(20, state));
        assertEquals("checkpoint", state.state);
        Ledger ledger = s.getLedger();
        assertEquals(10, ledger.getPaxosId());
        assertEquals(ballot, ledger.getInactiveLastTried());
        assertEquals(ballot, ledger.getInactiveNextBallot());
        assertEquals(0, s.getDeliveryQueueSize());
    }
    
    public void testRecoveryCheckpointAndChanges1() throws Exception {
        LoggingLedger loggingLedger = new LoggingLedger(10);
        BallotNumber ballot = loggingLedger.generateNextFastBallotNumber();
        loggingLedger.setInactiveLastTried(ballot);
        TestChangeLog newLog = new TestChangeLog();
        newLog.open(new TestChangeLogClient());
        newLog.writeCheckpoint(
                new PaxosCheckpoint(loggingLedger, TestStateManager.INITIAL,
                        new LinkedList<>(), Ledger.FIRST_COUNTER));
        newLog.writeChange(loggingLedger.flushLog());
        loggingLedger.setInactiveNextBallot(ballot);
        newLog.writeChange(loggingLedger.flushLog());
        newLog.writeCheckpoint(
                new PaxosCheckpoint(loggingLedger, "checkpoint",
                        new LinkedList<>(), 2));
        
        s = new Secretary(newLog.copy(), t);
        TestStateManager state = new TestStateManager();
        assertEquals(2, s.bind(20, state));
        assertEquals("checkpoint", state.state);
        Ledger ledger = s.getLedger();
        assertEquals(10, ledger.getPaxosId());
        assertEquals(ballot, ledger.getInactiveLastTried());
        assertEquals(ballot, ledger.getInactiveNextBallot());
        assertEquals(0, s.getDeliveryQueueSize());
    }
    
    public void testRecoveryCheckpointAndChanges2() throws Exception {
        LoggingLedger loggingLedger = new LoggingLedger(10);
        BallotNumber ballot = loggingLedger.generateNextFastBallotNumber();
        TestChangeLog newLog = new TestChangeLog();
        newLog.open(new TestChangeLogClient());
        newLog.writeCheckpoint(
                new PaxosCheckpoint(loggingLedger, TestStateManager.INITIAL,
                        new LinkedList<>(), Ledger.FIRST_COUNTER));
        newLog.writeCheckpoint(
                new PaxosCheckpoint(loggingLedger, "checkpoint",
                        new LinkedList<>(), 2));
        loggingLedger.setInactiveLastTried(ballot);
        newLog.writeChange(loggingLedger.flushLog());
        loggingLedger.setInactiveNextBallot(ballot);
        newLog.writeChange(loggingLedger.flushLog());
        
        s = new Secretary(newLog.copy(), t);
        TestStateManager state = new TestStateManager();
        assertEquals(2, s.bind(20, state));
        assertEquals("checkpoint", state.state);
        Ledger ledger = s.getLedger();
        assertEquals(10, ledger.getPaxosId());
        assertEquals(ballot, ledger.getInactiveLastTried());
        assertEquals(ballot, ledger.getInactiveNextBallot());
        assertEquals(0, s.getDeliveryQueueSize());
    }
    
    public void testRecoveryCheckpointAndQueue() throws Exception {
        LinkedList<byte[]> newQueue = new LinkedList<>();
        byte[] m = new byte[] {23};
        newQueue.add(m);
        TestChangeLog newLog = new TestChangeLog();
        newLog.open(new TestChangeLogClient());
        newLog.writeCheckpoint(
                new PaxosCheckpoint(new LoggingLedger(10), 
                        "checkpoint", newQueue, 1));

        s = new Secretary(newLog.copy(), t);
        TestStateManager state = new TestStateManager();
        assertEquals(1, s.bind(20, state));
        assertEquals("checkpoint", state.state);
        assertEquals(1, s.getDeliveryQueueSize());
        assertEquals(m[0], s.getDelivery()[0]);
        assertEquals(0, s.getDeliveryQueueSize());
    }
    
    public void testReBind() throws TreplicaException {
        Ledger ledger = s.getLedger();
        ledger.setInactiveLastTried(ledger.generateNextFastBallotNumber());
        s.synchronousFlush();
        s.takeCheckpoint("checkpoint", 2);
        BallotNumber ballot = ledger.generateNextFastBallotNumber();
        ledger.setInactiveLastTried(ballot);
        
        TestStateManager state = new TestStateManager();
        assertEquals(2, s.bind(20, state));
        assertEquals("checkpoint", state.state);
        ledger = s.getLedger();
        assertEquals(10, ledger.getPaxosId());
        assertEquals(ballot, ledger.getInactiveLastTried());
    }
    
    public void testTimedMessages() throws Exception {
        s.queueMessage("hello1");
        s.queueMessage("hello2", new TestTransportId(2));
        assertNull(t.popMemory());
        s.flush();
        Thread.sleep(20);
        assertEquals("hello1", t.popMemory().getPayload());
        assertEquals("hello2", t.popMemory().getPayload());
        assertNull(t.popMemory());
        assertEquals(0, s.getDeliveryQueueSize());
    }

    public void testTimedDeliveries() throws Exception {
        byte[] m1 = new byte[] {23};
        byte[] m2 = new byte[] {42};
        s.queueDelivery(m1);
        s.queueDelivery(m2);
        assertEquals(0, s.getDeliveryQueueSize());
        s.flush();
        Thread.sleep(20);
        assertEquals(2, s.getDeliveryQueueSize());
        assertEquals(m1, s.getDelivery());
        assertEquals(m2, s.getDelivery());
        assertEquals(0, s.getDeliveryQueueSize());
        assertNull(t.popMemory());
    }

    public void testTimedLedger() throws Exception {
        Ledger ledger = s.getLedger();
        BallotNumber ballot = ledger.generateNextFastBallotNumber();
        ledger.setInactiveLastTried(ballot);
        
        Secretary probe = new Secretary(log.copy(), t);
        TestStateManager state = new TestStateManager();
        assertEquals(Ledger.FIRST_COUNTER, probe.bind(20, state));
        assertEquals(TestStateManager.INITIAL, state.state);
        ledger = probe.getLedger();
        assertEquals(10, ledger.getPaxosId());
        assertNull(ledger.getInactiveLastTried());
        s.flush();
        Thread.sleep(20);
        probe = new Secretary(log.copy(), t);
        state = new TestStateManager();
        assertEquals(Ledger.FIRST_COUNTER, probe.bind(20, state));
        assertEquals(TestStateManager.INITIAL, state.state);
        ledger = probe.getLedger();
        assertEquals(10, ledger.getPaxosId());
        assertEquals(ballot, ledger.getInactiveLastTried());
        assertNull(t.popMemory());
        assertEquals(0, s.getDeliveryQueueSize());
    }

    public void testTimedAll() throws Exception {
        Ledger ledger = s.getLedger();
        BallotNumber ballot = ledger.generateNextFastBallotNumber();
        ledger.setInactiveLastTried(ballot);
        s.queueMessage("send");
        byte[] m = new byte[] {23};
        s.queueDelivery(m);
        
        Secretary probe = new Secretary(log.copy(), t);
        TestStateManager state = new TestStateManager();
        assertEquals(Ledger.FIRST_COUNTER, probe.bind(20, state));
        assertEquals(TestStateManager.INITIAL, state.state);
        ledger = probe.getLedger();
        assertEquals(10, ledger.getPaxosId());
        assertNull(ledger.getInactiveLastTried());
        assertNull(t.popMemory());
        assertEquals(0, s.getDeliveryQueueSize());
        s.flush();
        Thread.sleep(20);
        probe = new Secretary(log.copy(), t);
        state = new TestStateManager();
        assertEquals(Ledger.FIRST_COUNTER, probe.bind(20, state));
        assertEquals(TestStateManager.INITIAL, state.state);
        ledger = probe.getLedger();
        assertEquals(10, ledger.getPaxosId());
        assertEquals(ballot, ledger.getInactiveLastTried());
        assertEquals("send", t.popMemory().getPayload());
        assertEquals(1, s.getDeliveryQueueSize());
        assertEquals(m, s.getDelivery());
    }

    public void testTimedLedgerAndCheckpoint() throws Exception {
        Ledger ledger = s.getLedger();
        BallotNumber ballot = ledger.generateNextFastBallotNumber();
        ledger.setInactiveLastTried(ballot);
        s.synchronousFlush();
        s.takeCheckpoint("checkpoint", 2);
        TestChangeLogClient client = new TestChangeLogClient();
        log.copy().open(client);
        ledger = ((PaxosCheckpoint) client.lastCheckpoint).ledger;
        assertEquals(10, ledger.getPaxosId());
        assertEquals(ballot, ledger.getInactiveLastTried());
        assertEquals("checkpoint", ((PaxosCheckpoint) 
                client.lastCheckpoint).application);
        assertEquals(0, ((PaxosCheckpoint) 
                client.lastCheckpoint).deliveryQueue.size());
        assertEquals(2, ((PaxosCheckpoint) 
                client.lastCheckpoint).nextToDeliver);
        assertEquals(0, client.changes.size());
    }

}
