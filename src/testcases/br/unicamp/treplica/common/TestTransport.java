/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.common;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;


/**
 * This class implements a test transport. This transport supports only one
 * process, that will receive back all messages sent to all processes or
 * to itself. All other messages are discarded. However, it is possible to
 * recover a log of all messages <em>sent</em> using the 
 * <code>getMemory</code> and <code>popMemory</code> methods. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class TestTransport implements Transport {

    LinkedList<Message> messages;
    LinkedList<Message> memory;
    TestTransportId id;
    
    public TestTransport() {
        messages = new LinkedList<>();
        memory = new LinkedList<>();
        id = new TestTransportId(10);
    }

    @Override
    public TransportId getId() {
        return id;
    }
    
    @Override
    public synchronized Message receiveMessage() {
        return receiveMessage(0);
    }
    
    @Override
    public synchronized Message receiveMessage(int timeout) {
        if (messages.size() == 0) {
            try {
                this.wait(timeout);
            } catch (InterruptedException e) {
            }
        }
        if (messages.size() == 0) {
            return null; /* Timeout */
        }
        return messages.removeFirst();
    }
    
    @Override
    public synchronized void sendMessage(Serializable message) {
        sendMessage(message, null);
    }
    
    @Override
    public synchronized void sendMessage(Serializable message, TransportId destination) {
        Message m = new TestMessage(message, getId(), destination);
        if (destination == null || getId().equals(destination)) {
            messages.addLast(m);
            this.notify();
        }
        memory.addLast(m);
    }

    /**
     * Returns and remove from the message log the oldest message sent. <p>
     * 
     * @return the oldest message sent.
     */
    public synchronized Message popMemory() {
        try {
            return memory.removeFirst();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    /**
     * Returns a copy of the complete message log. <p>
     * 
     * @return a copy of the complete message log.
     */
    public synchronized List<Message> getMemory() {
        LinkedList<Message> list = new LinkedList<>();
        list.addAll(memory);
        return list;
    }
    
}
