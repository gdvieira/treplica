/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Luiz Eduardo Buzato
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.common;

import java.io.Serializable;

import junit.framework.TestCase;

public class ChangesStoreTest extends TestCase {

    private ChangesStore changesStore;
    
    @Override
    protected void setUp() {
        changesStore = new ChangesStore();
    }

    public void testExerciseChangeStore() {

        changesStore.resetChangeStore();

        assertFalse(changesStore.hasNextChange());

        long changesId = 0;

        for (int ci = 0; ci < 16; ci++) {
            changesStore.writeChange(changesId, "[" + changesId + "] change"
                    + ci);
        }

        changesStore.resetChangeStore();

        assertTrue(changesStore.hasNextChange());

        changesId = 1;

        for (int ci = 0; ci < 4; ci++) {
            changesStore.writeChange(changesId, "[" + changesId + "] change"
                    + ci);
        }

        changesId = 2;

        for (int ci = 0; ci < 1; ci++) {
            changesStore.writeChange(changesId, "[" + changesId + "] change"
                    + ci);
        }

        changesId = 3;

        for (int ci = 0; ci < 32; ci++) {
            changesStore.writeChange(changesId, "[" + changesId + "] change"
                    + ci);
        }

        changesStore.resetChangeStore();

        assertTrue(changesStore.hasNextChange());

        Serializable aChange = changesStore.nextChange();

        changesId = 0;
        int ci = 0;
        assertEquals("[" + changesId + "] change" + ci, aChange);

        int numberOfChangesReturned = 0;
        long nOfChanges = changesStore.resetChangeStore();
        while (changesStore.hasNextChange()) {
            aChange = changesStore.nextChange();
            numberOfChangesReturned++;
        }

        assertEquals(53, nOfChanges);
        assertEquals(53, numberOfChangesReturned);

        changesId = 0;
        changesStore.deleteChanges(changesId);

        numberOfChangesReturned = 0;
        nOfChanges = changesStore.resetChangeStore();
        while (changesStore.hasNextChange()) {
            aChange = changesStore.nextChange();
            numberOfChangesReturned++;
        }

        assertEquals(37, nOfChanges);
        assertEquals(37, numberOfChangesReturned);

        changesId = 2;
        changesStore.deleteChanges(changesId);

        numberOfChangesReturned = 0;
        nOfChanges = changesStore.resetChangeStore();
        while (changesStore.hasNextChange()) {
            aChange = changesStore.nextChange();
            numberOfChangesReturned++;
        }

        assertEquals(36, numberOfChangesReturned);
        assertEquals(36, nOfChanges);

        changesId = 2;

        for (int j = 0; j < 21; j++) {
            changesStore.writeChange(changesId, "[" + changesId
                    + "] new change" + ci);
        }

        numberOfChangesReturned = 0;
        nOfChanges = changesStore.resetChangeStore();
        while (changesStore.hasNextChange()) {
            aChange = changesStore.nextChange();
            numberOfChangesReturned++;
        }

        assertEquals(57, numberOfChangesReturned);
        assertEquals(57, nOfChanges);

        changesId = 3;
        changesStore.deleteChanges(changesId);

        changesId = 2;
        changesStore.deleteChanges(changesId);

        changesId = 1;
        changesStore.deleteChanges(changesId);

        changesStore.resetChangeStore();

        assertFalse(changesStore.hasNextChange());
    }
}
