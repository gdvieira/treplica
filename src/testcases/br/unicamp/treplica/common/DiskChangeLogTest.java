/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.common;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import junit.framework.TestCase;
import br.unicamp.treplica.TreplicaIOException;
import br.unicamp.treplica.TreplicaTestUtil;

public class DiskChangeLogTest extends TestCase {

    private String testDir;
    
    @Override
    protected void setUp() throws IOException {
        testDir = TreplicaTestUtil.createTestDirectory();
        assertNotNull(testDir);
    }
    
    @Override
    protected void tearDown() {
        assertTrue(TreplicaTestUtil.removeDirectory(testDir));
    }

    public void testWriteChangesNoCheckpoint() throws Exception {
        DiskChangeLog log = new DiskChangeLog(testDir);
        log.open(new TestChangeLogClient());
        log.writeChange("change1");
        log.writeChange("change2");
        
        ObjectInputStream stream = openInputStream(testDir, "0-0.changes");
        assertEquals("change1", stream.readObject());
        assertEquals("change2", stream.readObject());
        try {
            stream.readObject();
            fail("Read extra object.");
        } catch (EOFException e) {
        } finally {
            stream.close();
        }
    }
    
    public void testWriteCheckpointNoChanges() throws Exception {
        DiskChangeLog log = new DiskChangeLog(testDir);
        log.open(new TestChangeLogClient());
        log.writeCheckpoint("change1|change2");
        ObjectInputStream stream = openInputStream(testDir, "1.checkpoint");
        assertEquals("change1|change2", stream.readObject());
        try {
            stream.readObject();
            fail("Read extra object.");
        } catch (EOFException e) {
        } finally {
            stream.close();
        }

        log.writeCheckpoint("change1|change2|change3|change4");
        stream = openInputStream(testDir, "2.checkpoint");
        assertEquals("change1|change2|change3|change4", stream.readObject());
        try {
            stream.readObject();
            fail("Read extra object.");
        } catch (EOFException e) {
        } finally {
            stream.close();
        }
        assertFalse(exists(testDir, "1.checkpoint"));
    }
    
    public void testWriteChangesAndCheckpoints() throws Exception {
        DiskChangeLog log = new DiskChangeLog(testDir);
        log.open(new TestChangeLogClient());
        log.writeChange("change1");
        log.writeChange("change2");
        ObjectInputStream stream = openInputStream(testDir, "0-0.changes");
        assertEquals("change1", stream.readObject());
        assertEquals("change2", stream.readObject());
        stream.close();

        log.writeCheckpoint("change1|change2");
        log.writeChange("change3");
        log.writeChange("change4");
        stream = openInputStream(testDir, "1.checkpoint");
        assertEquals("change1|change2", stream.readObject());
        stream.close();
        stream = openInputStream(testDir, "1-0.changes");
        assertEquals("change3", stream.readObject());
        assertEquals("change4", stream.readObject());
        stream.close();
        assertFalse(exists(testDir, "0-0.changes"));

        log.writeCheckpoint("change1|change2|change3|change4");
        stream = openInputStream(testDir, "2.checkpoint");
        assertEquals("change1|change2|change3|change4", stream.readObject());
        stream.close();
        assertFalse(exists(testDir, "1.checkpoint"));
        assertFalse(exists(testDir, "1-0.changes"));
    }

    public void testReadChangesNoCheckpoint() throws Exception {
        ObjectOutputStream stream = openOutpuStream(testDir, "0-0.changes");
        stream.writeObject("change1");
        stream.writeObject("change2");
        stream.close();
        
        TestChangeLogClient client = new TestChangeLogClient();
        new DiskChangeLog(testDir).open(client);
        assertNull(client.lastCheckpoint);
        assertEquals(2, client.changes.size());
        assertEquals("change1", client.changes.get(0));
        assertEquals("change2", client.changes.get(1));
    }
    
    public void testReadCheckpointNoChanges() throws Exception {
        ObjectOutputStream stream = openOutpuStream(testDir, "1.checkpoint");
        stream.writeObject("change1|change2");
        stream.close();
        
        TestChangeLogClient client = new TestChangeLogClient();
        new DiskChangeLog(testDir).open(client);
        assertEquals("change1|change2", client.lastCheckpoint);
        assertEquals(0, client.changes.size());
    }
    
    public void testReadChangesAndCheckpoints1() throws Exception {
        ObjectOutputStream stream = openOutpuStream(testDir, "0-0.changes");
        stream.writeObject("change1");
        stream.writeObject("change2");
        stream.close();
        stream = openOutpuStream(testDir, "1.checkpoint");
        stream.writeObject("change1|change2");
        stream.close();
        
        TestChangeLogClient client = new TestChangeLogClient();
        new DiskChangeLog(testDir).open(client);
        assertEquals("change1|change2", client.lastCheckpoint);
        assertEquals(0, client.changes.size());
    }
    
    public void testReadChangesAndCheckpoints2() throws Exception {
        ObjectOutputStream stream = openOutpuStream(testDir, "0-0.changes");
        stream.writeObject("change1");
        stream.writeObject("change2");
        stream.close();
        stream = openOutpuStream(testDir, "1.checkpoint");
        stream.writeObject("change1|change2");
        stream.close();
        stream = openOutpuStream(testDir, "1-0.changes");
        stream.writeObject("change3");
        stream.writeObject("change4");
        stream.close();
        
        TestChangeLogClient client = new TestChangeLogClient();
        new DiskChangeLog(testDir).open(client);
        assertEquals("change1|change2", client.lastCheckpoint);
        assertEquals(2, client.changes.size());
        assertEquals("change3", client.changes.get(0));
        assertEquals("change4", client.changes.get(1));
    }
    
    public void testReadChangesAndCheckpoints3() throws Exception {
        ObjectOutputStream stream = openOutpuStream(testDir, "0-0.changes");
        stream.writeObject("change1");
        stream.writeObject("change2");
        stream.close();
        stream = openOutpuStream(testDir, "1.checkpoint");
        stream.writeObject("change1|change2");
        stream.close();
        stream = openOutpuStream(testDir, "1-0.changes");
        stream.writeObject("change3");
        stream.writeObject("change4");
        stream.close();
        stream = openOutpuStream(testDir, "2.checkpoint");
        stream.writeObject("change1|change2|change3|change4");
        stream.close();
        
        TestChangeLogClient client = new TestChangeLogClient();
        new DiskChangeLog(testDir).open(client);
        assertEquals("change1|change2|change3|change4", client.lastCheckpoint);
        assertEquals(0, client.changes.size());
    }
    
    public void testReadManyChanges() throws Exception {
        ObjectOutputStream stream = openOutpuStream(testDir, "1.checkpoint");
        stream.writeObject("change1|change2");
        stream.close();
        stream = openOutpuStream(testDir, "1-0.changes");
        stream.writeObject("change3");
        stream.writeObject("change4");
        stream.close();
        stream = openOutpuStream(testDir, "1-1.changes");
        stream.writeObject("change5");
        stream.writeObject("change6");
        stream.close();
        stream = openOutpuStream(testDir, "1-2.changes");
        stream.writeObject("change7");
        stream.close();
        
        TestChangeLogClient client = new TestChangeLogClient();
        new DiskChangeLog(testDir).open(client);
        assertEquals("change1|change2", client.lastCheckpoint);
        assertEquals(5, client.changes.size());
        assertEquals("change3", client.changes.get(0));
        assertEquals("change4", client.changes.get(1));
        assertEquals("change5", client.changes.get(2));
        assertEquals("change6", client.changes.get(3));
        assertEquals("change7", client.changes.get(4));
    }
    
    public void testWriteReadChangesNoCheckpoint() throws Exception {
        DiskChangeLog log = new DiskChangeLog(testDir);
        log.open(null);
        log.writeChange("change1");
        log.writeChange("change2");
        
        TestChangeLogClient client = new TestChangeLogClient();
        new DiskChangeLog(testDir).open(client);
        assertNull(client.lastCheckpoint);
        assertEquals(2, client.changes.size());
        assertEquals("change1", client.changes.get(0));
        assertEquals("change2", client.changes.get(1));
    }
    
    public void testWriteReadCheckpointNoChanges() throws Exception {
        DiskChangeLog log = new DiskChangeLog(testDir);
        log.open(null);
        log.writeCheckpoint("change1|change2");
        log.writeCheckpoint("change1|change2|change3|change4");
        
        assertFalse(exists(testDir, "1.checkpoint"));
        TestChangeLogClient client = new TestChangeLogClient();
        new DiskChangeLog(testDir).open(client);
        assertEquals("change1|change2|change3|change4", client.lastCheckpoint);
        assertEquals(0, client.changes.size());
    }
    
    public void testWriteReadChangesAndCheckpoints() throws Exception {
        DiskChangeLog log = new DiskChangeLog(testDir);
        log.open(null);
        log.writeChange("change1");
        log.writeChange("change2");
        log.writeCheckpoint("change1|change2");
        log.writeChange("change3");
        log.writeChange("change4");

        assertFalse(exists(testDir, "0-0.changes"));
        TestChangeLogClient client = new TestChangeLogClient();
        new DiskChangeLog(testDir).open(client);
        assertEquals("change1|change2", client.lastCheckpoint);
        assertEquals(2, client.changes.size());
        assertEquals("change3", client.changes.get(0));
        assertEquals("change4", client.changes.get(1));
    }
    
    public void testReadWriteChangesAndCheckpoints1() throws Exception {
        ObjectOutputStream outStream = openOutpuStream(testDir, "0-0.changes");
        outStream.writeObject("change1");
        outStream.writeObject("change2");
        outStream.close();
        outStream = openOutpuStream(testDir, "1.checkpoint");
        outStream.writeObject("change1|change2");
        outStream.close();
        
        TestChangeLogClient client = new TestChangeLogClient();
        DiskChangeLog log = new DiskChangeLog(testDir);
        log.open(client);
        log.writeChange("change3");
        log.writeChange("change4");
        ObjectInputStream inStream = openInputStream(testDir, "1-0.changes");
        assertEquals("change3", inStream.readObject());
        assertEquals("change4", inStream.readObject());
        inStream.close();

        log.writeCheckpoint("change1|change2|change3|change4");
        log.writeChange("change5");
        log.writeChange("change6");
        inStream = openInputStream(testDir, "2.checkpoint");
        assertEquals("change1|change2|change3|change4", inStream.readObject());
        inStream.close();
        inStream = openInputStream(testDir, "2-0.changes");
        assertEquals("change5", inStream.readObject());
        assertEquals("change6", inStream.readObject());
        inStream.close();
        assertFalse(exists(testDir, "0-0.changes"));
        assertFalse(exists(testDir, "1.checkpoint"));
        assertFalse(exists(testDir, "1-0.changes"));
    }
    
    public void testReadWriteChangesAndCheckpoints2() throws Exception {
        ObjectOutputStream outStream = openOutpuStream(testDir, "1.checkpoint");
        outStream.writeObject("start");
        outStream.close();
        outStream = openOutpuStream(testDir, "1-0.changes");
        outStream.writeObject("change1");
        outStream.writeObject("change2");
        outStream.close();
        
        TestChangeLogClient client = new TestChangeLogClient();
        DiskChangeLog log = new DiskChangeLog(testDir);
        log.open(client);
        log.writeChange("change3");
        log.writeChange("change4");

        ObjectInputStream inStream = openInputStream(testDir, "1-1.changes");
        assertEquals("change3", inStream.readObject());
        assertEquals("change4", inStream.readObject());
        try {
            inStream.readObject();
            fail("Read extra object.");
        } catch (EOFException e) {
        } finally {
            inStream.close();
        }
    }
    
    public void testOpenClose() throws Exception {
        DiskChangeLog log = new DiskChangeLog(testDir);
        try {
            log.writeChange("change1");
            fail("Write a change to a closed change log.");
        } catch (IllegalStateException e) {}
        try {
            log.writeCheckpoint("change1|change2");
            fail("Write a checkpoint to a closed change log.");
        } catch (IllegalStateException e) {}
        try {
            log.close();
            fail("Close a closed change log.");
        } catch (IllegalStateException e) {}
        log.open(null);
        try {
            log.open(null);
            fail("Open a open change log.");
        } catch (IllegalStateException e) {}
        log.writeChange("change1");
        log.writeChange("change2");
        log.close();
        try {
            log.writeChange("change3");
            fail("Write a change to a closed change log.");
        } catch (IllegalStateException e) {}
        try {
            log.writeCheckpoint("change1|change2|change3");
            fail("Write a checkpoint to a closed change log.");
        } catch (IllegalStateException e) {}
        try {
            log.close();
            fail("Close a closed change log.");
        } catch (IllegalStateException e) {}
        TestChangeLogClient client = new TestChangeLogClient();
        log.open(client);
        assertNull(client.lastCheckpoint);
        assertEquals(2, client.changes.size());
        assertEquals("change1", client.changes.get(0));
        assertEquals("change2", client.changes.get(1));
    }
    
    public void testError() throws Exception {
        DiskChangeLog log = new DiskChangeLog(testDir);
        log.open(null);
        log.writeCheckpoint("change1|change2");
        
        assertTrue(TreplicaTestUtil.removeDirectory(testDir));
        try {
            log.writeCheckpoint("change1|change2|change3|change4");
            fail("Undetected exception");
        } catch (TreplicaIOException e) {}
        assertFalse(log.isOpen());
    }
    
    private ObjectInputStream openInputStream(String dir, String name) throws IOException {
        String path = dir + File.separator + name;
        return new ObjectInputStream(new FileInputStream(path));
    }
    
    private ObjectOutputStream openOutpuStream(String dir, String name) throws IOException {
        String path = dir + File.separator + name;
        return new ObjectOutputStream(new FileOutputStream(path));
    }

    private boolean exists(String dir, String name) {
        String path = dir + File.separator + name;
        return new File(path).exists();
    }
}
