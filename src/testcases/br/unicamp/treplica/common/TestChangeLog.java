/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.common;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;

import br.unicamp.treplica.TreplicaIOException;
import br.unicamp.treplica.TreplicaSerializationException;


/**
 * This class implements a test change log, that won't try to make its changes
 * persistent.  <p>  
 * 
 * @author Gustavo Maciel Dias Vieira
 */
public class TestChangeLog implements ChangeLog {
    
    private LinkedList<Serializable> state;
    
    public TestChangeLog() {
        state = new LinkedList<>();
        state.addLast(null);
    }
    
    @Override
    public void open(ChangeLogClient client) throws TreplicaIOException,
            TreplicaSerializationException {
        Iterator<Serializable> i = state.iterator();
        Serializable checkpoint = i.next();
        if (checkpoint != null) {
            client.processCheckpoint(Marshall.unmarshall((byte[]) checkpoint));
        }
        while (i.hasNext()) {
            Serializable change = i.next();
            client.processChange(Marshall.unmarshall((byte[]) change));
        }
    }

    @Override
    public void close() throws TreplicaIOException {
    }

    @Override
    public boolean isOpen() {
        return true;
    }

    @Override
    public void writeChange(Serializable change) throws TreplicaIOException {
        try {
            state.addLast(Marshall.marshall(change));
        } catch (TreplicaSerializationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeCheckpoint(Serializable checkpoint)
        throws TreplicaIOException {
        state = new LinkedList<>();
        try {
            state.addLast(Marshall.marshall(checkpoint));
        } catch (TreplicaSerializationException e) {
            e.printStackTrace();
        }
    }

    public TestChangeLog copy() {
        TestChangeLog log = new TestChangeLog();
        log.state.clear();
        log.state.addAll(state);
        return log;
    }
    
}
