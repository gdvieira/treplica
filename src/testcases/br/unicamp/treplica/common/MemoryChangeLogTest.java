/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Luiz Eduardo Buzato
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.common;

import java.util.ArrayList;
import java.io.Serializable;

import br.unicamp.treplica.TreplicaIOException;

import junit.framework.TestCase;

public class MemoryChangeLogTest extends TestCase {

    private static final String SERVER = "127.0.0.1:6667";
    private MemoryChangeLogServer server;
    private TestChangeLogClient testChangeLogClient;
    private MemoryChangeLog log;

    @Override
    public void setUp() throws TreplicaIOException {
        server = new MemoryChangeLogServer(6667);
        testChangeLogClient = new TestChangeLogClient();
        log = new MemoryChangeLog(SERVER);
        log.open(testChangeLogClient);
    }

    @Override
    public void tearDown() throws Exception {
        server.shutdown();
        server.join();
    }

    public void testLoadEmptyChangeLog() throws Exception {
        Serializable lastCheckpoint = testChangeLogClient.lastCheckpoint;

        assertNull(lastCheckpoint);

        ArrayList<Serializable> changes = testChangeLogClient.changes;

        assertTrue(changes.isEmpty());

        log.close();
    }

    public void testWriteChangesNoCheckpoint() throws Exception {
        log.writeChange("change1");
        log.writeChange("change2");
        log.close();

        testChangeLogClient = new TestChangeLogClient();
        log = new MemoryChangeLog(SERVER);
        log.open(testChangeLogClient);

        assertNull(testChangeLogClient.lastCheckpoint);
        ArrayList<Serializable> changes = testChangeLogClient.changes;
        assertEquals(2, changes.size());
        assertTrue(changes.contains("change1"));
        assertTrue(changes.contains("change2"));

        log.close();
    }

    public void testWriteChangesAndCheckpoints() throws Exception {
        testChangeLogClient.changes.clear();

        int checkpointStamp = 1;

        log.writeCheckpoint("checkpoint" + checkpointStamp);

        for (int changeStamp = 0; changeStamp < 4; changeStamp++) {
            log.writeChange("[" + checkpointStamp + "] change " + changeStamp);
        }

        testChangeLogClient = new TestChangeLogClient();
        log = new MemoryChangeLog(SERVER);
        log.open(testChangeLogClient);

        Serializable lastCheckpoint = testChangeLogClient.lastCheckpoint;
        assertEquals("checkpoint1", lastCheckpoint);

        ArrayList<Serializable> changes = testChangeLogClient.changes;
        assertEquals(4, changes.size());
        for (int changeStamp = 0; changeStamp < 4; changeStamp++) {
            assertTrue(changes.contains("[" + checkpointStamp + "] change "
                    + changeStamp));
        }

        checkpointStamp++;

        log.writeCheckpoint("checkpoint" + checkpointStamp);

        for (int changeStamp = 0; changeStamp < 2; changeStamp++) {
            log.writeChange("[" + checkpointStamp + "] change " + changeStamp);
        }

        testChangeLogClient = new TestChangeLogClient();
        log = new MemoryChangeLog(SERVER);
        log.open(testChangeLogClient);

        lastCheckpoint = testChangeLogClient.lastCheckpoint;
        assertEquals("checkpoint2", lastCheckpoint);

        changes = testChangeLogClient.changes;
        assertEquals(2, changes.size());
        for (int changeStamp = 0; changeStamp < 2; changeStamp++) {
            assertTrue(changes.contains("[" + checkpointStamp + "] change "
                    + changeStamp));
        }

        log.close();
    }

}
