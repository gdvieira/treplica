/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica;

import java.io.File;
import java.io.IOException;

/**
 * This class holds some common test utilities. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class TreplicaTestUtil {
    
    public static String createTestDirectory() throws IOException {
        File dir = File.createTempFile("treplica-test", null);
        dir.delete();
        if (dir.mkdir()) {
            dir.deleteOnExit();
            return dir.getAbsolutePath();
        }
        return null;
    }
    
    public static boolean removeDirectory(String name) {
        File dir = new File(name);
        if (dir.exists()) {
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (!files[i].delete()) {
                    return false;
                }
            }
            return dir.delete();
        }
        return true;
    }
    
}
