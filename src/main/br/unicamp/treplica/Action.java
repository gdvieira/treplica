/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica;

import java.io.Serializable;

/**
 * This interface represents an action executed on the state machine. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public interface Action extends Serializable {
    
    /**
     * Executes this action on the provided state machine. <p>
     *  
     * @param stateMachine the state machine.
     * @return the result of the execution of this action.
     * @throws Exception if an exception occurs in this action.
     */
    Object executeOn(Object stateMachine) throws Exception;

}
