/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica;

import java.io.Serializable;

/**
 * This interface represents the keeper of the deterministic state to be
 * made persistent by use of a persistent queue. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public interface StateManager {
    
    /**
     * Takes a snapshot of the current state of the deterministic object
     * held by this state manager. The serializable object returned must be
     * sufficient to reconstruct the state in another context. <p>
     * 
     * @return a serializable object containing the current state held by
     *         this object.
     */
    Serializable getState();

    /**
     * Restores a snapshot and uses it to replace the current state of the
     * deterministic object held by this state manager. The serializable
     * object provided is the same as some object returned to a previous
     * call to <code>getState()</code>. <p>
     * 
     * @param state the state snapshot to be restored.
     */
    void setState(Serializable state);
}
