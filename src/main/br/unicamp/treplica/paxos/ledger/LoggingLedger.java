/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 * Copyright © 2015 Vinícius A. Reis
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos.ledger;

import java.util.LinkedList;

import br.unicamp.treplica.paxos.BallotNumber;
import br.unicamp.treplica.paxos.Proposal;
import br.unicamp.treplica.paxos.Vote;

/**
 * This class implements a version of the Paxos ledger that registers the
 * changes that should be made persistent. <p>
 * 
 * @author Gustavo Maciel Dias Vieira
 */
public class LoggingLedger implements Ledger {
    private static final long serialVersionUID = 3518490027955837534L;
    
    private TransientLedger ledger;
    private LinkedList<LedgerChange> log;
    
    /**
     * Creates a new ledger. <p>
     * 
     * @param paxosId the id of this Paxos instance, used to create ballots.
     */
    public LoggingLedger(int paxosId) {
        ledger = new TransientLedger(paxosId);
        log = new LinkedList<>();
    }
    
    /**
     * Returns and clears the current log of changes of this ledger. <p>
     * 
     * @return the current log of changes to this ledger.
     */
    public LedgerChange[] flushLog() {
        LedgerChange[] oldLog = new LedgerChange[log.size()];
        oldLog = log.toArray(oldLog);
        log = new LinkedList<>();
        return oldLog;
    }

    /**
     * Replays the provided log of changes in this logging ledger. The
     * replayed operations won't appear in the current log of changes of this
     * ledger.<p>
     * 
     * @param changeLog a log of changes to be replayed.
     */
    public void replayLog(LedgerChange[] changeLog) {
        for (int i = 0; i < changeLog.length; i++) {
            changeLog[i].executeOnLedger(ledger);
        }
    }
    
    @Override
    public int getPaxosId() {
        return ledger.getPaxosId();
    }

    @Override
    public long getZeroTime(){
        return ledger.getZeroTime();
    }

    @Override
    public BallotNumber getLastTried(long decreeNumber) {
        return ledger.getLastTried(decreeNumber);
    }
    
    @Override
    public void setLastTried(long decreeNumber, BallotNumber ballot) {
        performChange(new SetLastTried(decreeNumber, ballot));
    }

    @Override
    public BallotNumber getInactiveLastTried() {
        return ledger.getInactiveLastTried();
    }
    
    @Override
    public void setInactiveLastTried(BallotNumber ballot) {
        performChange(new SetInactiveLastTried(ballot));
    }
    
    @Override
    public BallotNumber getNextBallot(long decreeNumber) {
        return ledger.getNextBallot(decreeNumber);
    }
    
    @Override
    public void setNextBallot(long decreeNumber, BallotNumber ballot) {
        performChange(new SetNextBallot(decreeNumber, ballot));
    }
    
    @Override
    public BallotNumber getInactiveNextBallot() {
        return ledger.getInactiveNextBallot();
    }
    
    @Override
    public void setInactiveNextBallot(BallotNumber ballot) {
        performChange(new SetInactiveNextBallot(ballot));
    }
    
    @Override
    public Vote getPrevVote(long decreeNumber) {
        return ledger.getPrevVote(decreeNumber);
    }
    
    @Override
    public void setPrevVote(long decreeNumber, Vote vote) {
        performChange(new SetPrevVote(decreeNumber, vote));
    }
    
    @Override
    public Proposal read(long decreeNumber) {
        return ledger.read(decreeNumber);
    }

    @Override
    public void write(long decreeNumber, Proposal proposal) {
        performChange(new Write(decreeNumber, proposal));
    }
    
    @Override
    public BallotNumber generateNextClassicBallotNumber() {
        return (BallotNumber) performChange(new GenerateNextClassicBallotNumber());
    }
    
    @Override
    public BallotNumber generateNextFastBallotNumber() {
        return (BallotNumber) performChange(new GenerateNextFastBallotNumber());
    }
    
    @Override
    public void ensureHigherBallotNumber(BallotNumber ballot) {
        performChange(new EnsureHigherBallotNumber(ballot));
    }

    @Override
    public void erase(long decreeNumber) {
        performChange(new Erase(decreeNumber));
    }

    @Override
    public long lastActiveDecree() {
        return ledger.lastActiveDecree();
    }
    
    @Override
    public long firstUndecidedDecree() {
        return ledger.firstUndecidedDecree();
    }
    
    @Override
    public long lastDecidedDecree() {
        return ledger.lastDecidedDecree();
    }

    @Override
    public boolean isActive(long decreeNumber) {
        return ledger.isActive(decreeNumber);
    }
    
    @Override
    public boolean isDecided(long decreeNumber) {
        return ledger.isDecided(decreeNumber);
    }

    @Override
    public boolean isPresent(long decreeNumber) {
        return ledger.isPresent(decreeNumber);
    }

    /**
     * Performs a change on this ledger. <p>
     * 
     * @param change the change.
     * @return the return value of the change.
     * @see LedgerChange#executeOnLedger(Ledger)
     */
    protected Object performChange(LedgerChange change) {
        log.addLast(change);
        return change.executeOnLedger(ledger);
    }
}
