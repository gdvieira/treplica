/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos.ledger;

import br.unicamp.treplica.paxos.Vote;

/**
 * This class implements the change performed by the
 * <code>setPrevVote(long, Vote)</code> method to the ledger. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 * @see Ledger#setPrevVote(long, Vote)
 */
public class SetPrevVote implements LedgerChange {
    private static final long serialVersionUID = -6798621809214867900L;
    
    private long decreeNumber;
    private Vote vote;
    
    /**
     * Creates the change with the method parameters. <p>
     * 
     * @param decreeNumber the decree number.
     * @param vote the vote.
     */
    public SetPrevVote(long decreeNumber, Vote vote) {
        this.decreeNumber = decreeNumber;
        this.vote = vote;
    }
    
    @Override
    public Object executeOnLedger(Ledger ledger) {
        ledger.setPrevVote(decreeNumber, vote);
        return null;
    }

}