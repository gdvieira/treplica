/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 * Copyright © 2015 Vinícius A. Reis
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos.ledger;

import java.io.Serializable;
import java.util.ArrayList;

import br.unicamp.treplica.paxos.BallotNumber;
import br.unicamp.treplica.paxos.Proposal;
import br.unicamp.treplica.paxos.Vote;

/**
 * This class implements a transient version of the Paxos ledger. <p>
 * 
 * @author Gustavo Maciel Dias Vieira
 */
public class TransientLedger implements Ledger {
    private static final long serialVersionUID = -4696672903888980180L;
    
    private int paxosId;
    private long zeroTime;
    private long nextBallotCounter;
    private ArrayList<Decree> decrees;
    private BallotNumber inactiveLastTried;
    private BallotNumber inactiveNextBallot;
    private long firstPresentDecree;
    private long lastActiveDecree;
    private long firstUndecidedDecree;
    private long lastDecidedDecree;
    
    /**
     * Creates a new ledger. <p>
     * 
     * @param paxosId the id of this Paxos instance, used to create ballots.
     */
    public TransientLedger(int paxosId) {
        this.paxosId = paxosId;
        zeroTime = System.currentTimeMillis();
        nextBallotCounter = Ledger.FIRST_COUNTER;
        decrees = new ArrayList<>();
        inactiveLastTried = null;
        inactiveNextBallot = null;
        firstPresentDecree = Ledger.FIRST_COUNTER;
        lastActiveDecree = NULL_COUNTER;
        firstUndecidedDecree = Ledger.FIRST_COUNTER;
        lastDecidedDecree = NULL_COUNTER;
    }
    
    @Override
    public int getPaxosId() {
        return paxosId;
    }

    @Override
    public long getZeroTime(){
        return zeroTime;
    }
    
    @Override
    public BallotNumber getLastTried(long decreeNumber) {
        BallotNumber ballot = inactiveLastTried;
        if (isActive(decreeNumber)) {
            ballot = decrees.get(decreeNumberToIndex(decreeNumber)).lastTried;
        }
        return ballot;
    }
    
    @Override
    public void setLastTried(long decreeNumber, BallotNumber ballot) {
        activateDecree(decreeNumber);
        Decree decree = decrees.get(decreeNumberToIndex(decreeNumber));
        decree.lastTried = ballot;
    }

    @Override
    public BallotNumber getInactiveLastTried() {
        return inactiveLastTried;
    }
    
    @Override
    public void setInactiveLastTried(BallotNumber ballot) {
        inactiveLastTried = ballot;
    }
    
    @Override
    public BallotNumber getNextBallot(long decreeNumber) {
        BallotNumber ballot = inactiveNextBallot;
        if (isActive(decreeNumber)) {
            ballot = decrees.get(decreeNumberToIndex(decreeNumber)).nextBallot;
        }
        return ballot;
    }
    
    @Override
    public void setNextBallot(long decreeNumber, BallotNumber ballot) {
        activateDecree(decreeNumber);
        Decree decree = decrees.get(decreeNumberToIndex(decreeNumber));
        decree.nextBallot = ballot;
    }
    
    @Override
    public BallotNumber getInactiveNextBallot() {
        return inactiveNextBallot;
    }
    
    @Override
    public void setInactiveNextBallot(BallotNumber ballot) {
        inactiveNextBallot = ballot;
    }
    
    @Override
    public Vote getPrevVote(long decreeNumber) {
        Vote vote = null;
        if (isActive(decreeNumber)) {
            vote = decrees.get(decreeNumberToIndex(decreeNumber)).prevVote;
        }
        return vote;
    }
    
    @Override
    public void setPrevVote(long decreeNumber, Vote vote) {
        activateDecree(decreeNumber);
        Decree decree = decrees.get(decreeNumberToIndex(decreeNumber));
        decree.prevVote = vote;
    }
    
    @Override
    public Proposal read(long decreeNumber) {
        Proposal proposal = null;
        if (isActive(decreeNumber)) {
            proposal = decrees.get(decreeNumberToIndex(decreeNumber)).finalProposal;
        }
        return proposal;
    }

    @Override
    public void write(long decreeNumber, Proposal proposal) {
        activateDecree(decreeNumber);
        Decree decree = decrees.get(decreeNumberToIndex(decreeNumber));
        decree.finalProposal = proposal;
        decree.isDecided = true;
        if (decreeNumber == firstUndecidedDecree) {
            firstUndecidedDecree++;
            while (isDecided(firstUndecidedDecree)) {
                firstUndecidedDecree++;
            }
        }
        if (decreeNumber > lastDecidedDecree) {
            lastDecidedDecree = decreeNumber;
        }
    }
    
    @Override
    public BallotNumber generateNextClassicBallotNumber() {
        return new BallotNumber(paxosId, nextBallotCounter++);
    }
    
    @Override
    public BallotNumber generateNextFastBallotNumber() {
        return new BallotNumber(paxosId, nextBallotCounter++, true);
    }
    
    @Override
    public void ensureHigherBallotNumber(BallotNumber ballot) {
        if (ballot.getCounter() >= nextBallotCounter) {
            nextBallotCounter = ballot.getCounter() + 1;
        }
    }

    @Override
    public void erase(long decreeNumber) {
        int decreeIndex = decreeNumberToIndex(decreeNumber);
        if (decreeIndex < 0) {
            return;
        }
        if (decreeIndex > decrees.size()) {
            decreeIndex = decrees.size();
        }
        decrees.subList(0, decreeIndex).clear();
        firstPresentDecree = decreeNumber;
        if (lastActiveDecree < decreeNumber - 1) {
            lastActiveDecree = decreeNumber - 1;
        }
        if (firstUndecidedDecree < decreeNumber) {
            firstUndecidedDecree = decreeNumber;
        }
        if (lastDecidedDecree < decreeNumber - 1) {
            lastDecidedDecree = decreeNumber - 1;
        }
    }

    @Override
    public long lastActiveDecree() {
        return lastActiveDecree;
    }
    
    @Override
    public long firstUndecidedDecree() {
        return firstUndecidedDecree;
    }
    
    @Override
    public long lastDecidedDecree() {
        return lastDecidedDecree;
    }

    @Override
    public boolean isActive(long decreeNumber) {
        int index = decreeNumberToIndex(decreeNumber);
        return index < decrees.size() && decrees.get(index) != null;
    }
    
    @Override
    public boolean isDecided(long decreeNumber) {
        boolean decided = false;
        if (isActive(decreeNumber)) {
            decided = decrees.get(decreeNumberToIndex(decreeNumber)).isDecided;
        }
        return decided;
    }
    
    @Override
    public boolean isPresent(long decreeNumber) {
        return decreeNumberToIndex(decreeNumber) >= 0;
    }

    /**
     * Activates a decree, initializing its data to values consistent with
     * previous promises. <p> 
     * 
     * @param decreeNumber the decree number.
     */
    private void activateDecree(long decreeNumber) {
        if (! isActive(decreeNumber)) {
            Decree decree = new Decree();
            decree.lastTried = inactiveLastTried;
            decree.nextBallot = inactiveNextBallot;
            decree.prevVote = null;
            decree.finalProposal = null;
            decree.isDecided = false;
            int index = decreeNumberToIndex(decreeNumber);
            for (int i = decrees.size(); i <= index; i++) {
                decrees.add(null);
            }
            decrees.set(index, decree);
            if (decreeNumber > lastActiveDecree) {
                lastActiveDecree = decreeNumber;
            }
        }
    }
    
    /**
     * Converts a decree number to the index in local data structure used
     * to store it. <p>
     *   
     * @param decreeNumber the decree number.
     * @return the index in local data structure used to store a decree.
     */
    private int decreeNumberToIndex(long decreeNumber) {
        return (int) (decreeNumber - firstPresentDecree);
    }
    
    /**
     * This class holds the data for a decree. <p>
     */
    protected class Decree implements Serializable {
        private static final long serialVersionUID = -5274189340573380319L;
        protected BallotNumber lastTried;
        protected BallotNumber nextBallot;
        protected Vote prevVote;
        protected Proposal finalProposal;
        protected boolean isDecided;
    }
}
