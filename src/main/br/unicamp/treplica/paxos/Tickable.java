/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2015 Vinícius A. Reis
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos;

import br.unicamp.treplica.TreplicaIOException;
import br.unicamp.treplica.TreplicaSerializationException;

/**
 * This interface defines a Tickable element, a rudimentary abstraction of
 * a logical clock. <p>
 *
 * @author Vinícius A. Reis
 */
public interface Tickable {

    /**
     * Tick the logical clock of this object.
     * The behavior triggered by a clock tick is object-specific. <p>
     * 
     * @return the next time to tick this object.
     * @throws TreplicaIOException 
     * @throws TreplicaSerializationException 
     */
    long processTick() throws TreplicaIOException, TreplicaSerializationException;
}
