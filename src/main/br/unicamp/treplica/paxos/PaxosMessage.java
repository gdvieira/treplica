/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos;

import java.io.Serializable;
import java.util.HashMap;

import br.unicamp.treplica.TreplicaInvalidFieldRuntimeException;

/**
 * This class implements all messages sent by the Paxos protocol. <p>
 * 
 * @author Gustavo Maciel Dias Vieira
 */
public class PaxosMessage implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static final int LEADER               = 0;
    public static final int PASS                 = 1;
    public static final int NEXT_BALLOT          = 2;
    public static final int NEXT_BALLOT_INACTIVE = 3;
    public static final int LAST_VOTE            = 4;
    public static final int LAST_VOTE_INACTIVE   = 5;
    public static final int BEGIN_BALLOT         = 6;
    public static final int VOTED                = 7;
    public static final int SUCCESS              = 8;
    public static final int LARGER_BALLOT        = 9;
    public static final int ANY                  = 10;
    public static final int STATUS               = 11;

    private int sender;
    private int type;
    private HashMap<Character, Object> fields;
    private static final char COUNTER_INDEX       = 'c';
    private static final char DECREE_NUMBER_INDEX = 'd';
    private static final char BALLOT_NUMBER_INDEX = 'b';
    private static final char VOTE_INDEX          = 'v';
    private static final char PROPOSAL_INDEX      = 'p';
   
    /**
     * Creates a Paxos message. <p>
     * 
     * @param sender the Paxos id of this message sender.
     * @param type the type of this message.
     */
    private PaxosMessage(int sender, int type) {
        this.sender = sender;
        this.type = type;
        fields = new HashMap<>();
    }

    /**
     * Returns the Paxos id of this message sender. <p>
     * 
     * @return the Paxos id of this message sender.
     */
    public int getSender() {
        return sender;
    }

    /**
     * Returns the type of this message. <p>
     * 
     * @return the type of this message.
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the counter of this message. <p>
     *
     * @param counter the counter of this message.
     */
    private void setCounter(long counter) {
        fields.put(COUNTER_INDEX, counter);
    }

    /**
     * Returns the counter of this message. <p>
     *
     * @return the counter of this message.
     */
    public long getCounter() {
        if (!fields.containsKey(COUNTER_INDEX)) {
            throw new TreplicaInvalidFieldRuntimeException();
        }
        return (long) fields.get(COUNTER_INDEX);
    }

    /**
     * Sets the decree number this message refers to. <p>
     *
     * @param decreeNumber the decree number this message refers to.
     */
    private void setDecreeNumber(long decreeNumber) {
        fields.put(DECREE_NUMBER_INDEX, decreeNumber);
    }

    /**
     * Returns the decree number this message refers to. <p>
     * 
     * @return the decree number this message refers to.
     */
    public long getDecreeNumber() {
        if (!fields.containsKey(DECREE_NUMBER_INDEX)) {
            throw new TreplicaInvalidFieldRuntimeException();
        }
        return (long) fields.get(DECREE_NUMBER_INDEX);
    }

    /**
     * Sets the ballot number of this message. <p>
     *
     * @param ballotNumber the ballot number of this message.
     */
    public void setBallotNumber(BallotNumber ballotNumber) {
        fields.put(BALLOT_NUMBER_INDEX, ballotNumber);
    }

    /**
     * Returns the ballot number of this message. <p>
     * 
     * @return the ballot number of this message.
     */
    public BallotNumber getBallotNumber() {
        if (!fields.containsKey(BALLOT_NUMBER_INDEX)) {
            throw new TreplicaInvalidFieldRuntimeException();
        }
        return (BallotNumber) fields.get(BALLOT_NUMBER_INDEX);
    }

    /**
     * Sets the vote of this message. <p>
     *
     * @param vote the vote of this message.
     */
    public void setVote(Vote vote) {
        fields.put(VOTE_INDEX, vote);
    }

    /**
     * Returns the vote of this message. <p>
     * 
     * @return the vote of this message.
     */
    public Vote getVote() {
        if (!fields.containsKey(VOTE_INDEX)) {
            throw new TreplicaInvalidFieldRuntimeException();
        }
        return (Vote) fields.get(VOTE_INDEX);
    }

    /**
     * Returns the proposal of this message. <p>
     *
     * @param proposal the proposal of this message.
     */
    public void setProposal(Proposal proposal) {
        fields.put(PROPOSAL_INDEX, proposal);
    }

    /**
     * Returns the proposal of this message. <p>
     *
     * @return the proposal of this message.
     */
    public Proposal getProposal() {
        if (!fields.containsKey(PROPOSAL_INDEX)) {
            throw new TreplicaInvalidFieldRuntimeException();
        }
        return (Proposal) fields.get(PROPOSAL_INDEX);
    }

    @Override
    public String toString() {
        StringBuffer string = new StringBuffer(Integer.toHexString(sender));
        switch (type) {
        case LEADER:
            string.append(":LEA:");
            string.append(getCounter());
            break;
        case PASS:
            string.append(":PAS:");
            string.append(getDecreeNumber());
            string.append(":");
            string.append(getProposal());
            break;
        case NEXT_BALLOT:
            string.append(":NBT:");
            string.append(getDecreeNumber());
            string.append(":");
            string.append(getBallotNumber());
            break;
        case NEXT_BALLOT_INACTIVE:
            string.append(":NBI:");
            string.append(getBallotNumber());
            break;
        case LAST_VOTE:
            string.append(":LVT:");
            string.append(getDecreeNumber());
            string.append(":");
            string.append(getBallotNumber());
            string.append(":");
            string.append(getVote());
            break;
        case LAST_VOTE_INACTIVE:
            string.append(":LVI:");
            string.append(getDecreeNumber());
            string.append(":");
            string.append(getBallotNumber());
            break;
        case BEGIN_BALLOT:
            string.append(":BBT:");
            string.append(getDecreeNumber());
            string.append(":");
            string.append(getBallotNumber());
            string.append(":");
            string.append(getProposal());
            break;
        case VOTED:
            string.append(":VOT:");
            string.append(getDecreeNumber());
            string.append(":");
            string.append(getBallotNumber());
            string.append(":");
            string.append(getProposal());
            break;
        case SUCCESS:
            string.append(":SUC:");
            string.append(getDecreeNumber());
            string.append(":");
            string.append(getProposal());
            break;
        case LARGER_BALLOT:
            string.append(":LBT:");
            string.append(getBallotNumber());
            break;
        case ANY:
            string.append(":ANY:");
            string.append(getDecreeNumber());
            string.append(":");
            string.append(getBallotNumber());
            break;
        case STATUS:
            string.append(":STA:");
            string.append(getCounter());
            break;
        }

        return string.toString();
    }

    /**
     * Creates a Paxos message of type LEADER. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param counter the counter of this leader message.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createLeader(int sender, long counter) {
        PaxosMessage message = new PaxosMessage(sender, LEADER);
        message.setCounter(counter);
        return message;
    }

    /**
     * Creates a Paxos message of type PASS. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param decreeNumber the decree number this message refers to.
     * @param proposal the proposal.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createPass(int sender, long decreeNumber,
            Proposal proposal) {
        PaxosMessage message = new PaxosMessage(sender, PASS);
        message.setDecreeNumber(decreeNumber);
        message.setProposal(proposal);
        return message;
    }

    /**
     * Creates a Paxos message of type NEXT_BALLOT. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param decreeNumber the decree number this message refers to.
     * @param ballotNumber the ballot number.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createNextBallot(int sender, long decreeNumber,
            BallotNumber ballotNumber) {
        PaxosMessage message = new PaxosMessage(sender, NEXT_BALLOT);
        message.setDecreeNumber(decreeNumber);
        message.setBallotNumber(ballotNumber);
        return message;
    }

    /**
     * Creates a Paxos message of type NEXT_BALLOT_INACTIVE. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param ballotNumber the ballot number.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createNextBallotInactive(int sender,
            BallotNumber ballotNumber) {
        PaxosMessage message = new PaxosMessage(sender, NEXT_BALLOT_INACTIVE);
        message.setBallotNumber(ballotNumber);
        return message;
    }

    /**
     * Creates a Paxos message of type LAST_VOTE. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param decreeNumber the decree number this message refers to.
     * @param ballotNumber the ballot number.
     * @param vote the vote.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createLastVote(int sender, long decreeNumber,
            BallotNumber ballotNumber, Vote vote) {
        PaxosMessage message = new PaxosMessage(sender, LAST_VOTE);
        message.setDecreeNumber(decreeNumber);
        message.setBallotNumber(ballotNumber);
        message.setVote(vote);
        return message;
    }

    /**
     * Creates a Paxos message of type LAST_VOTE_INACTIVE. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param decreeNumber the decree number this message refers to.
     * @param ballotNumber the ballot number.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createLastVoteInactive(int sender,
            long decreeNumber, BallotNumber ballotNumber) {
        PaxosMessage message = new PaxosMessage(sender, LAST_VOTE_INACTIVE);
        message.setDecreeNumber(decreeNumber);
        message.setBallotNumber(ballotNumber);
        return message;
    }

    /**
     * Creates a Paxos message of type BEGIN_BALLOT. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param decreeNumber the decree number this message refers to.
     * @param ballotNumber the ballot number.
     * @param proposal the proposal.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createBeginBallot(int sender, long decreeNumber,
            BallotNumber ballotNumber, Proposal proposal) {
        PaxosMessage message = new PaxosMessage(sender, BEGIN_BALLOT);
        message.setDecreeNumber(decreeNumber);
        message.setBallotNumber(ballotNumber);
        message.setProposal(proposal);
        return message;
    }

    /**
     * Creates a Paxos message of type VOTED. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param decreeNumber the decree number this message refers to.
     * @param ballotNumber the ballot number.
     * @param proposal the proposal.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createVoted(int sender, long decreeNumber,
            BallotNumber ballotNumber, Proposal proposal) {
        PaxosMessage message = new PaxosMessage(sender, VOTED);
        message.setDecreeNumber(decreeNumber);
        message.setBallotNumber(ballotNumber);
        message.setProposal(proposal);
        return message;
    }

    /**
     * Creates a Paxos message of type SUCCESS. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param decreeNumber the decree number this message refers to.
     * @param proposal the proposal.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createSuccess(int sender, long decreeNumber,
            Proposal proposal) {
        PaxosMessage message = new PaxosMessage(sender, SUCCESS);
        message.setDecreeNumber(decreeNumber);
        message.setProposal(proposal);
        return message;
    }

    /**
     * Creates a Paxos message of type LARGER_BALLOT. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param ballotNumber the ballot number.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createLargerBallot(int sender,
            BallotNumber ballotNumber) {
        PaxosMessage message = new PaxosMessage(sender, LARGER_BALLOT);
        message.setBallotNumber(ballotNumber);
        return message;
    }

    /**
     * Creates a Paxos message of type ANY. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param decreeNumber the decree number this message refers to.
     * @param ballotNumber the ballot number.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createAny(int sender, long decreeNumber,
            BallotNumber ballotNumber) {
        PaxosMessage message = new PaxosMessage(sender, ANY);
        message.setDecreeNumber(decreeNumber);
        message.setBallotNumber(ballotNumber);
        return message;
    }

    /**
     * Creates a Paxos message of type STATUS. <p>
     *
     * @param sender the Paxos id of this message sender.
     * @param counter the state counter of this status message.
     * @return a new Paxos message with the provided parameters.
     */
    public static PaxosMessage createStatus(int sender, long counter) {
        PaxosMessage message = new PaxosMessage(sender, STATUS);
        message.setCounter(counter);
        return message;
    }

}
