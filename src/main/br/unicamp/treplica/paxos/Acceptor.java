/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos;

import br.unicamp.treplica.common.TransportId;
import br.unicamp.treplica.paxos.ledger.Ledger;

/**
 * This class implements the acceptor abstraction of Paxos. The acceptor
 * takes part of ballots, casting votes according to its ledger. <p>
 * 
 * @author Gustavo Maciel Dias Vieira
 */
public class Acceptor {
    
    private Secretary secretary;
    
    /**
     * Creates an acceptor that accesses the ledger and sends messages through
     * the provided secretary. <p>
     * 
     * @param secretary the secretary that handles I/O.
     */
    public Acceptor(Secretary secretary) {
        this.secretary = secretary;
    }
    
    
    /**
     * Processes an acceptor message received by the protocol. <p>
     * 
     * @param message the message received.
     * @param sender the message sender.
     */
    public void processMessage(PaxosMessage message, TransportId sender) {
        switch (message.getType()) {
        case PaxosMessage.NEXT_BALLOT:
            processNextBallot(message, sender);
            break;
        case PaxosMessage.NEXT_BALLOT_INACTIVE:
            processNextBallotInactive(message, sender);
            break;
        case PaxosMessage.BEGIN_BALLOT:
            processBeginBallot(message, sender);
            break;
        }
    }

    /**
     * Processes a <code>PaxosMessage.NEXT_BALLOT</code> message received by
     * the acceptor. Messages referring to a smaller ballot number than the
     * current next ballot number are ignored if created by the same process, if
     * not, a <code>PaxosMessage.LARGER_BALLOT</code> is created. Messages
     * referring to a non present decree are ignored. <p>
     * 
     * @param message the message received.
     * @param sender the message sender.
     */
    private void processNextBallot(PaxosMessage message, TransportId sender) {
        Ledger ledger = secretary.getLedger();
        long decree = message.getDecreeNumber();
        BallotNumber ballot = message.getBallotNumber();
        if (!ledger.isPresent(decree)) {
            return;
        }
        PaxosMessage reply = null;
        BallotNumber nextBallot = ledger.getNextBallot(decree);
        if (ledger.isDecided(decree)) {
            reply = PaxosMessage.createSuccess(secretary.getPaxosId(), decree,
                    ledger.read(decree));
        } else if (nextBallot == null || nextBallot.compareTo(ballot) < 0) {
            ledger.setNextBallot(decree, ballot);
            reply = PaxosMessage.createLastVote(secretary.getPaxosId(), decree,
                    ballot, ledger.getPrevVote(decree));
        } else if (nextBallot.getPaxosId() != ballot.getPaxosId()) {
            reply = PaxosMessage.createLargerBallot(secretary.getPaxosId(),
                    nextBallot);
        }
        if (reply != null) {
            secretary.queueMessage(reply, sender);
        }
    }

    /**
     * Processes a <code>PaxosMessage.NEXT_BALLOT_INACTIVE</code> message
     * received by the acceptor. Messages referring to a smaller ballot number
     * than the current inactive next ballot number are ignored if created by
     * the same process, if not, a <code>PaxosMessage.LARGER_BALLOT</code> is
     * sent. <p>
     * 
     * @param message the message received.
     * @param sender the message sender.
     */
    private void processNextBallotInactive(PaxosMessage message, TransportId sender) {
        Ledger ledger = secretary.getLedger();
        BallotNumber nextBallotInactive = ledger.getInactiveNextBallot(); 
        if (nextBallotInactive == null ||
            nextBallotInactive.compareTo(message.getBallotNumber()) < 0) {
            ledger.setInactiveNextBallot(message.getBallotNumber());
            PaxosMessage reply = PaxosMessage.createLastVoteInactive(
                    secretary.getPaxosId(), ledger.lastActiveDecree() + 1,
                    message.getBallotNumber());
            secretary.queueMessage(reply, sender);
        } else if (nextBallotInactive.getPaxosId() != 
                   message.getBallotNumber().getPaxosId()) {
            PaxosMessage reply = PaxosMessage.createLargerBallot(
                    secretary.getPaxosId(), nextBallotInactive);
            secretary.queueMessage(reply, sender);
        }
    }

    /**
     * Processes a <code>PaxosMessage.BEGIN_BALLOT</code> message received by
     * the acceptor. Messages referring to a non present decree or to the wrong
     * ballot number are ignored. <p>
     * 
     * @param message the message received.
     * @param sender the message sender.
     */
    private void processBeginBallot(PaxosMessage message, TransportId sender) {
        Ledger ledger = secretary.getLedger();
        long decreeNumber = message.getDecreeNumber();
        BallotNumber ballot = message.getBallotNumber();
        if (!ledger.isPresent(decreeNumber)) {
            return;
        }
        BallotNumber nextBallot = ledger.getNextBallot(decreeNumber);
        Vote prevVote = ledger.getPrevVote(decreeNumber);
        if ((nextBallot == null || ! (nextBallot.compareTo(ballot) > 0)) &&
            (prevVote == null || prevVote.getBallotNumber().compareTo(ballot) < 0)) {
            Vote vote = new Vote(ballot, message.getProposal());
            if (nextBallot == null || nextBallot.compareTo(ballot) < 0) {
                ledger.setNextBallot(decreeNumber, ballot);
            }
            ledger.setPrevVote(decreeNumber, vote);
            Proposal proposalReference = message.getProposal(); 
            if (proposalReference != null) {
                proposalReference = 
                    new Proposal(proposalReference.getId(), null);
            }
            PaxosMessage reply = PaxosMessage.createVoted(
                    secretary.getPaxosId(), decreeNumber, ballot,
                    proposalReference);
            secretary.queueMessage(reply);
        } else if (nextBallot != null && nextBallot.compareTo(ballot) > 0 &&
                ! ballot.isFast()) {
            PaxosMessage reply = PaxosMessage.createLargerBallot(
                    secretary.getPaxosId(), nextBallot);
            secretary.queueMessage(reply, sender);
        }
    }

}
