/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 * Copyright © 2015 Vinícius A. Reis
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos;

import br.unicamp.treplica.TreplicaIOException;
import br.unicamp.treplica.TreplicaSerializationException;

/**
 * This class implements a leader election service. This service is a
 * $\Omega$ failure detector. It just guarantees that exists a process p and
 * a time t such as after t all processes trust p. <p>
 * 
 * The current implementation uses a delta to indicate the interval between
 * expected leader I-am-alive messages. <p>
 * 
 * FIXME: It might be interesting to completely implement Chen's algorithm
 *        with delta and ni, to determine QoS for this failure detector.
 * 
 * @author Gustavo Maciel Dias Vieira
 */
public class Election  implements Tickable {
    
    private Secretary secretary;
    private int delta;
    private ElectionClient client;
    private long myCounter;
    private int leader;
    private long leaderCounter;
    private boolean knowsLeader;
    private long leaderTimestamp;

    /**
     * Creates a leader election service that sends messages through the
     * provided secretary and has the provided delta. <p>
     * 
     * @param secretary the secretary that handles I/O.
     * @param delta the delta between I-am-alive messages in milliseconds.
     * @param client the client of this leader election service.
     */
    public Election(Secretary secretary, int delta, ElectionClient client) {
        this.secretary = secretary;
        this.delta = delta;
        this.client = client;
        myCounter = 0;
        leader = 0;
        leaderCounter = 0;
        knowsLeader = false;
        leaderTimestamp = System.currentTimeMillis();
    }
    
    /**
     * Returns the current leader. However, this instance of the election
     * service might not know of any leader yet, so it is necessary to check
     * if a leader has been elected by calling <code>knowsLeader()</code>. <p>
     * 
     * @return the current leader.
     * @see Election#knowsLeader()
     */
    public int getLeader() {
        return leader;
    }
    
    /**
     * Indicates if this instance knows a leader. <p>
     * 
     * @return <code>true</code> if this instance knows a leader;
     *         <code>false</code> otherwise.
     */
    public boolean knowsLeader() {
        return knowsLeader;
    }
    

    /**
     * Processes a leader election message received by the protocol. <p>
     * 
     * @param message the message received.
     * @throws TreplicaSerializationException if a serialization error occurs.
     * @throws TreplicaIOException if an I/O error occurs.
     */
    public void processMessage(PaxosMessage message)
            throws TreplicaIOException, TreplicaSerializationException {
        if (! knowsLeader 
                || (message.getCounter() > leaderCounter
                        && message.getSender() != leader) 
                || (message.getCounter() == leaderCounter
                        && message.getSender() > leader)) {
            leader = message.getSender();
            knowsLeader = true;
            client.leaderChange(leader);
        }
        if (message.getSender() == leader) {
            leaderCounter = message.getCounter();
            leaderTimestamp = System.currentTimeMillis();
        }
    }

    /**
     * Process a clock tick generated by the protocol. For every tick, this
     * method sends an I-am-alive message if it is a leader or it elects itself 
     * as a leader if it has not received a message from the leader. <p>
     * 
     * @throws TreplicaSerializationException if a serialization error occurs.
     * @throws TreplicaIOException if an I/O error occurs.
     */
    @Override
    public long processTick()
            throws TreplicaIOException, TreplicaSerializationException {
        long nextWakeTime = this.delta / (long) 2;
        if (secretary.getPaxosId() == leader) {
            PaxosMessage message =
                    PaxosMessage.createLeader(secretary.getPaxosId(),
                                              ++myCounter);
            leaderCounter = myCounter;
            secretary.queueMessage(message);
        } else { 
            if ((System.currentTimeMillis() - leaderTimestamp) > delta) {
                leader = secretary.getPaxosId();
                leaderCounter = myCounter;
                knowsLeader = true;
                client.leaderChange(leader);
            } else {
                nextWakeTime = delta;
            }
        }
        nextWakeTime += System.currentTimeMillis();   
        return nextWakeTime;
    }
}
