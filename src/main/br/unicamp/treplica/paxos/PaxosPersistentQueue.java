/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 * Copyright © 2015 Vinícius A. Reis
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.unicamp.treplica.PersistentQueue;
import br.unicamp.treplica.StateManager;
import br.unicamp.treplica.TreplicaException;
import br.unicamp.treplica.TreplicaIOException;
import br.unicamp.treplica.TreplicaSerializationException;
import br.unicamp.treplica.common.ChangeLog;
import br.unicamp.treplica.common.Marshall;
import br.unicamp.treplica.common.Message;
import br.unicamp.treplica.common.Transport;
import br.unicamp.treplica.common.TransportId;

/**
 * This class implements a persistent queue using the Paxos protocol. <p>
 * 
 * @author Gustavo Maciel Dias Vieira
 */
public class PaxosPersistentQueue implements PersistentQueue, ElectionClient {

    protected Logger logger;
    protected int roundtrip;
    private int maxProcesses;
    private boolean fast;
    protected Transport transport;
    private StateManager applicationState;
    protected Secretary secretary;
    protected Election election;
    protected Coordinator coordinator;
    protected Acceptor acceptor;
    protected Learner learner;
    private GlobalDeliveryState globalDeliveryState;
    private Thread mainLoop;
    
    /**
     * Creates a new Paxos persistent queue. <p> 
     * 
     * @param roundtrip the expected time for a response to a message to be
     *                  received, including processing, in milliseconds.
     * @param maxProcesses the maximum number of processes bound to this queue.
     * @param fast if this queue will use fast Paxos ballots.
     * @param transport the transport this queue will use to send messages.
     * @param log the change log this queue will use for persistence.
     */
    public PaxosPersistentQueue(int roundtrip, int maxProcesses, boolean fast,
                                Transport transport, ChangeLog log) {
        logger = LoggerFactory.getLogger(this.getClass());
        this.roundtrip = roundtrip;
        this.maxProcesses = maxProcesses;
        this.fast = fast;
        this.transport = transport;
        applicationState = null;
        secretary = new Secretary(log, transport);
        election = null;
        coordinator = null;
        acceptor = null;
        learner = null;
        globalDeliveryState = null;
        mainLoop = null;
    }
    
    @Override
    public void bind(StateManager manager)
            throws TreplicaIOException, TreplicaSerializationException {
        synchronized (secretary) {
            applicationState = manager;
            long nextToDeliver = secretary.bind(new Random().nextInt(),
                    applicationState);
            int classicMajority = (maxProcesses / 2) + 1;
            int fastMajority = maxProcesses - (maxProcesses / 4);
//            int classicMajority = ((2 * maxProcesses) / 3) + 1;
//            int fastMajority = classicMajority;
            election = new Election(secretary, 2 * roundtrip, this);
            coordinator = new Coordinator(secretary, classicMajority,
                    fastMajority, 2 * roundtrip, fast);
            acceptor = new Acceptor(secretary);
            learner = new Learner(secretary, maxProcesses, classicMajority,
                    fastMajority, 2 * roundtrip, nextToDeliver);
            globalDeliveryState = new GlobalDeliveryState(maxProcesses);
            if (mainLoop == null) {
                mainLoop = new Thread(new MainLoop(), "Paxos Main Thread");
                mainLoop.setDaemon(true);
                mainLoop.start();
            }
        }
    }
    
    @Override
    public void enqueue(Serializable message)
            throws TreplicaIOException, TreplicaSerializationException {
        byte[] sMessage = Marshall.marshall(message);
        synchronized (secretary) {
            learner.order(sMessage);
            secretary.flush();
        }
    }

    @Override
    public Serializable dequeue() 
            throws TreplicaIOException, TreplicaSerializationException {
        return Marshall.unmarshall(secretary.getDelivery());
    }

    @Override
    public void checkpoint() throws TreplicaIOException {
        synchronized (secretary) {
            secretary.getLedger().erase(
                    globalDeliveryState.getGlobalNextToDeliver());
            secretary.synchronousFlush();
            secretary.takeCheckpoint(applicationState.getState(),
                    learner.getNextToDeliver());
        }
    }
    
    @Override
    public void leaderChange(int newLeader)
            throws TreplicaIOException, TreplicaSerializationException {
        if (secretary.getPaxosId() == newLeader) {
            coordinator.activate();
        } else {
            coordinator.deactivate();
        }
    }

    /**
     * Processes a queue level message. <p>
     *
     * @param message the message received.
     */
    protected void processMessage(PaxosMessage message) {
        globalDeliveryState.updateGlobalDeliveries(message.getSender(),
                message.getCounter());
    }

    /**
     * This class implements the main loop for this Paxos persistent
     * queue. It listens to messages, receives them, and forwards them to
     * correct component, while keeping track of time and sending clock
     * ticks to some components at a constant rate. <p>
     */
    protected class MainLoop implements Runnable {
        
        private int step;
        private long deadline;
        private LinkedList<TickableTime> tickableList;
        
        private class TickableTime {
            public Tickable tickableObject;
            public long wakeUpTime;
            
            public TickableTime(Tickable tickableObject, long wakeUpTime){
                this.tickableObject = tickableObject;
                this.wakeUpTime = wakeUpTime;
            }
        }
        
        protected MainLoop() {
            step = roundtrip;
            deadline = System.currentTimeMillis() + step;
            tickableList = new LinkedList<>();
            register(election, deadline);
            register(coordinator, deadline);
            register(learner, deadline);
        }
        
        @Override
        public void run() {
            while (true) {
                try {
                    long now = System.currentTimeMillis();
                    if (now < deadline) {
                        Message message = 
                            transport.receiveMessage((int) (deadline - now));
                        if (message != null) {
                            processMessage(message);
                        }
                    } else {
                        deadline = processTick();
                    }
                } catch (TreplicaException e) {
                    throw new RuntimeException(
                            "Exception in PaxosPersistentQueue main thread. "
                            + "Thread stopped!", e);
                }
            }
        }

        private void processMessage(Message message)
                throws TreplicaIOException, TreplicaSerializationException {
            TransportId sender = message.getSender();
            PaxosMessage payload = (PaxosMessage) message.getPayload();
            if (payload.getType() != PaxosMessage.LEADER) {
                logger.debug("Received message: {} From: {}", payload, sender);
            } else {
                logger.trace("Received message: {} From: {}", payload, sender);
            }
            synchronized (secretary) {
                switch (payload.getType()) {
                case PaxosMessage.LEADER:
                    election.processMessage(payload);
                    break;
                case PaxosMessage.PASS:
                case PaxosMessage.LAST_VOTE:
                case PaxosMessage.LAST_VOTE_INACTIVE:
                case PaxosMessage.LARGER_BALLOT:
                    coordinator.processMessage(payload, sender);
                    break;
                case PaxosMessage.NEXT_BALLOT:
                case PaxosMessage.NEXT_BALLOT_INACTIVE:
                    acceptor.processMessage(payload, sender);
                    break;
                case PaxosMessage.BEGIN_BALLOT:
                    learner.processMessage(payload);
                    acceptor.processMessage(payload, sender);
                    break;
                case PaxosMessage.ANY:
                case PaxosMessage.VOTED:
                case PaxosMessage.SUCCESS:
                    learner.processMessage(payload);
                    break;
                case PaxosMessage.STATUS:
                    PaxosPersistentQueue.this.processMessage(payload);
                    break;
                }
                secretary.flush();
            }
        }
        
        public void register(Tickable tickableObject, long timestamp) {
            TickableTime t = new TickableTime(tickableObject, timestamp);
            tickableList.add(t);
        }        

        private long processTick() throws TreplicaIOException,
                TreplicaSerializationException {
            long nextWakeUpTime = Long.MAX_VALUE;
            long now = System.currentTimeMillis();

            synchronized (secretary) {
                for (TickableTime i: tickableList) {
                    if (i.wakeUpTime <= now) {
                        long newWakeUpTime = i.tickableObject.processTick();
                        i.wakeUpTime = newWakeUpTime;
                    }
                    if (i.wakeUpTime < nextWakeUpTime)
                        nextWakeUpTime = i.wakeUpTime;
                }
                secretary.flush();
            }
            return nextWakeUpTime;
        }
    }
}
