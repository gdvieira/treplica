/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2022 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos;

import java.util.HashMap;

import br.unicamp.treplica.paxos.ledger.Ledger;

/**
 * This class keeps information about the global delivery state in all known
 * replicas. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class GlobalDeliveryState {

    private int maxProcesses;
    private HashMap<Integer, Long> globalDeliveries;

    /**
     * Creates a new global delivery state, considering the provided maximum
     * number of replicas. <p>
     *
     * @param maxProcesses the maximum number of replicas.
     */
    public GlobalDeliveryState(int maxProcesses) {
        this.maxProcesses = maxProcesses;
        globalDeliveries = new HashMap<>();
    }

    public void updateGlobalDeliveries(int paxosId, long nextToDeliver) {
        globalDeliveries.put(paxosId, nextToDeliver);
    }

    public long getGlobalNextToDeliver() {
        long globalNextToDeliver = Ledger.FIRST_COUNTER;
        if (globalDeliveries.size() == maxProcesses) {
            globalNextToDeliver = Integer.MAX_VALUE;
            for (long nextToDeliver : globalDeliveries.values()) {
                if (nextToDeliver < globalNextToDeliver) {
                    globalNextToDeliver = nextToDeliver;
                }
            }
        }
        return globalNextToDeliver;
    }
}
