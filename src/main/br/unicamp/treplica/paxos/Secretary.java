/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 * Copyright © 2015 Vinícius A. Reis
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.paxos;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import br.unicamp.treplica.StateManager;
import br.unicamp.treplica.TreplicaException;
import br.unicamp.treplica.TreplicaIOException;
import br.unicamp.treplica.TreplicaSerializationException;
import br.unicamp.treplica.common.ChangeLog;
import br.unicamp.treplica.common.ChangeLogClient;
import br.unicamp.treplica.common.Transport;
import br.unicamp.treplica.common.TransportId;
import br.unicamp.treplica.paxos.ledger.Ledger;
import br.unicamp.treplica.paxos.ledger.LedgerChange;
import br.unicamp.treplica.paxos.ledger.LoggingLedger;

/**
 * The secretary implements an I/O helper object that concentrates and orders
 * I/O operations. The purpose of this class is to take costly I/O out
 * of the main synchronization block of the algorithm, allowing operation
 * grouping and operation parallelism. Specifically, it will flush ledger
 * changes (through the change log), send protocol messages (through the
 * transport) and deliver application messages (to a delivery queue). <p>
 * 
 * Ledger changes are the synchronization point of this class. They must be
 * flushed strictly in the order they were created, and have dependent
 * actions that can only be performed after the flush completes. The
 * secretary enforces this behavior. A flush of the secretary will flush the
 * ledger, wait for it to finish and perform all dependent actions queued 
 * after the last flush. The secretary may optimize these I/O operations by
 * grouping them or dispatching them concurrently, but it always respects
 * the described ordering behavior.  <p>  
 * 
 * @author Gustavo Maciel Dias Vieira
 */
public class Secretary  {

    protected ChangeLog log;
    protected Transport transport;
    protected LinkedList<byte[]> deliveryQueue;
    protected LoggingLedger ledger;
    private LinkedList<Action> actionQueue;
    protected Object flushedQueuesMonitor;
    protected LinkedList<LedgerChange> flushedChangeQueue;
    protected LinkedList<Action> flushedActionQueue;
    
    /**
     * Creates a new secretary that handles ledger changes through the
     * provided change log, sends messages through the provided transport and
     * delivers application messages to the provided delivery queue. <p>
     * 
     * @param log the change log that handles ledger changes.
     * @param transport the transport to send protocol messages.
     */
    public Secretary(ChangeLog log, Transport transport) {
        this.log = log;
        this.transport = transport;
        this.deliveryQueue = new LinkedList<>();
        ledger = null;
        actionQueue = null;
        flushedQueuesMonitor = new Object();
        flushedChangeQueue = new LinkedList<>();
        flushedActionQueue = new LinkedList<>();
        Thread writer = new Thread(new IOWriter(), "Paxos I/O Thread");
        writer.setDaemon(true);
        writer.start();
    }
    
    /**
     * Binds and synchronizes this secretary (including the ledger) and the
     * application to the current state of the change log. <p>
     * 
     * @param paxosId the id of this Paxos instance
     * @param state the state manager for the application.
     * @return the index of the next decree to be delivered to the application.
     * @throws TreplicaIOException if an I/O error occurs.
     * @throws TreplicaSerializationException if a serialization error occurs.
     */
    public long bind(int paxosId, StateManager state)
            throws TreplicaIOException, TreplicaSerializationException {
        if (ledger != null) {
            synchronousFlush();
        }
        SecretaryChangeLogClient client = new SecretaryChangeLogClient(state);
        ledger = new LoggingLedger(paxosId);
        log.open(client);
        actionQueue = new LinkedList<>();
        if (!client.checkpointRestored) {
            takeCheckpoint(state.getState(), client.nextToDeliver);
        }
        return client.nextToDeliver;
    }
    
    /**
     * Returns the ledger currently held by this secretary. This ledger is
     * managed by the scribe of this secretary. <p>
     * 
     * @return the ledger currently held by this secretary.
     */
    public Ledger getLedger() {
        return ledger;
    }
    
    /**
     * Returns the unique Paxos id of the ledger used by this secretary. <p>
     * 
     * @return the unique Paxos id of the ledger.
     */
    public int getPaxosId() {
        return getLedger().getPaxosId();
    }
    
    /**
     * Queues a protocol message multicast send, to be performed when the
     * secretary flushes the next batch of ledger changes. <p>
     * 
     * @param message the message to be sent.
     */
    public void queueMessage(Serializable message) {
        queueMessage(message, null);
    }
    
    /**
     * Queues a protocol message unicast send, to be performed when the
     * secretary flushes the next batch of ledger changes. <p>
     * 
     * @param message the message to be sent.
     * @param destination the destination.
     */
    public void queueMessage(Serializable message, TransportId destination) {
        actionQueue.addLast(new SendMessageAction(message, destination));
    }
    
    /**
     * Queues an application message delivery, to be performed when the
     * secretary flushes the next batch of ledger changes. <p>
     * 
     * @param message the byte array containing the marshalled message.
     */
    public void queueDelivery(byte[] message) {
        actionQueue.addLast(new DeliverMessageAction(message));
    }
    
    /**
     * Flushes the changes made to the underlying ledger. Once the changes
     * are completely flushed to stable storage the secretary dequeues and
     * performs all actions queued up to when they were flushed. <p>
     */
    public void flush() {
        synchronized (flushedQueuesMonitor) {
            for (Action action : actionQueue) {
                flushedActionQueue.addLast(action);
            }
            actionQueue = new LinkedList<>();
            LedgerChange[] ledgerChanges = ledger.flushLog();
            for (int i = 0; i < ledgerChanges.length; i++) {
                flushedChangeQueue.addLast(ledgerChanges[i]);
            }
            flushedQueuesMonitor.notify();
        }
    }

    /**
     * Synchronously flushes the changes made to the underlying ledger and
     * immediately dequeues and performs all actions queued up to now. <p>
     */
    public void synchronousFlush() {
        synchronized (flushedQueuesMonitor) {
            flush();
            try {
                flushedQueuesMonitor.wait();
            } catch (InterruptedException e) {
            }
        }
    }
    
    /**
     * Sends a unicast message, without queueing it. <p>
     * 
     * @param message the message to be sent. 
     * @param destination the destination.
     * @throws TreplicaIOException 
     * @throws TreplicaSerializationException 
     */
    public void sendMessage(Serializable message, TransportId destination)
                throws TreplicaIOException, TreplicaSerializationException {
            transport.sendMessage(message, destination);
    }
    
    /**
     * Sends a multicast message, without queueing it. <p>
     * 
     * @param message the message to be sent. 
     * @throws TreplicaIOException 
     * @throws TreplicaSerializationException 
     */
    public void sendMessage(Serializable message)
                throws TreplicaIOException, TreplicaSerializationException {
            transport.sendMessage(message);
    }

    /**
     * Takes a checkpoint immediately. This method assumes the queued ledger
     * changes and actions are flushed and that the objects being saved as part
     * of the checkpoint won't change during the checkpointing process. The
     * current state of the ledger and of the delivery queue will be saved also,
     * and restored when appropriate. <p>
     *
     * @param applicationState the application level checkpoint to be saved.
     * @param nextToDeliver the next decree to be delivered.
     * @throws TreplicaIOException if an I/O error occurs while saving the
     *                             checkpoint.
     */
    public void takeCheckpoint(Serializable applicationState, long nextToDeliver)
            throws TreplicaIOException {
        synchronized (deliveryQueue) {
            PaxosCheckpoint checkpoint = new PaxosCheckpoint(ledger,
                    applicationState, deliveryQueue, nextToDeliver);
            log.writeCheckpoint(checkpoint);
        }
    }

    /**
     * Returns the current size of the application message delivery queue.
     *
     * @return the current size of the application message delivery queue.
     */
    public int getDeliveryQueueSize() {
        synchronized (deliveryQueue) {
            return deliveryQueue.size();
        }
    }

    /**
     * Returns the next application message delivery, as previously queued. If
     * the delivery queue is empty, this method blocks until there is a
     * delivery available.
     *
     * @return the next application message to be delivered.
     */
    public byte[] getDelivery() {
        byte[] message;
        synchronized (deliveryQueue) {
            while (deliveryQueue.size() == 0) {
                try {
                    deliveryQueue.wait();
                } catch (InterruptedException e) {
                }
            }
            message = deliveryQueue.removeFirst();
        }
        return message;
    }

    /**
     * This class implements the I/O flushing thread of this secretary.
     * It waits for some work on the flushed queues and writes the flushed
     * ledger changes and executes the actions related to them. <p>
     */
    protected class IOWriter implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    LinkedList<LedgerChange> changes = null;
                    LinkedList<Action> actions = null;
                    synchronized (flushedQueuesMonitor) {
                        if (flushedChangeQueue.size() == 0 && 
                            flushedActionQueue.size() == 0) {
                            try {
                                flushedQueuesMonitor.notify();
                                flushedQueuesMonitor.wait();
                            } catch (InterruptedException e) {
                            }
                        }
                        if (flushedChangeQueue.size() != 0) {
                            changes = flushedChangeQueue;
                            flushedChangeQueue = new LinkedList<>();
                        }
                        if (flushedActionQueue.size() != 0) {
                            actions = flushedActionQueue;
                            flushedActionQueue = new LinkedList<>();
                        }
                    }
                    sync(changes, actions);
                } catch (TreplicaException e) {
                    throw new RuntimeException(
                            "Exception in Secretary I/O thread. "
                            + "Thread stopped!", e);
                }
            }
        }
        
        /**
         * Writes the provided changes through the scribe and executes
         * the provided actions. <p>
         * 
         * @param changes the changes to be written.
         * @param actions the actions to be performed.
         * @throws TreplicaIOException if an I/O error occurs.
         * @throws TreplicaSerializationException if a serialization error occurs.
         */
        private void sync(List<LedgerChange> changes, LinkedList<Action> actions)
                throws TreplicaIOException, TreplicaSerializationException {
            if (changes != null && changes.size() > 0) {
                LedgerChange[] ledgerChanges = new LedgerChange[changes.size()];
                ledgerChanges = changes.toArray(ledgerChanges);
                log.writeChange(ledgerChanges);
            }
            if (actions != null) {
                while (actions.size() > 0) {
                    Action action = actions.removeFirst();
                    action.execute();
                }
            }
        }

    }

    /**
     * This interface represents a queued action of the secretary. <p>
     */
    private interface Action {
        /**
         * Executes this action. <p> 
         * 
         * @throws TreplicaIOException if an I/O error occurs.
         * @throws TreplicaSerializationException if a serialization error occurs.
         */
        void execute() throws TreplicaIOException, TreplicaSerializationException;
    }
    
    /**
     * This class implements a send message action. <p>
     */
    private class SendMessageAction implements Action {
        
        private Serializable message;
        private TransportId destination;
        
        protected SendMessageAction(Serializable message, TransportId destination) {
            this.message = message;
            this.destination = destination;
        }
        
        @Override
        public void execute()
                throws TreplicaIOException, TreplicaSerializationException {
            transport.sendMessage(message, destination);
        }
    }

    /**
     * This class implements a deliver message action. <p>
     */
    private class DeliverMessageAction implements Action {
        
        private byte[] message;
        
        protected DeliverMessageAction(byte[] message) {
            this.message = message;
        }
        
        @Override
        public void execute()
                throws TreplicaIOException, TreplicaSerializationException {
            synchronized (deliveryQueue) {
                deliveryQueue.addLast(message);
                deliveryQueue.notify();
            }
        }
    }

    /**
     * This class acts as a client to the change log, to participate in
     * recovery. <p>
     * 
     * @see ChangeLogClient
     */
    protected class SecretaryChangeLogClient implements ChangeLogClient {
        
        private StateManager appState;
        protected long nextToDeliver;
        protected boolean checkpointRestored;
        
        public SecretaryChangeLogClient(StateManager state) {
            appState = state;
            nextToDeliver = Ledger.FIRST_COUNTER;
            checkpointRestored = false;
        }
        
        @Override
        public void processChange(Serializable change) {
            ledger.replayLog((LedgerChange[]) change);
        }

        @Override
        public void processCheckpoint(Serializable checkpoint) {
            PaxosCheckpoint paxosCkpt = (PaxosCheckpoint) checkpoint;
            appState.setState(paxosCkpt.application);
            ledger = paxosCkpt.ledger;
            deliveryQueue = paxosCkpt.deliveryQueue;
            nextToDeliver = paxosCkpt.nextToDeliver;
            checkpointRestored = true;
        }
    }
    
}
