/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2022 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica;

/**
 * This class implements a Treplica invalid field runtime exception. This
 * exception is thrown when an invalid or undefined field is accessed. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class TreplicaInvalidFieldRuntimeException extends
        TreplicaRuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new <code>TreplicaInvalidFieldRuntimeException</code>
     * object with <code>null</code> as its detail message. <p>
     */
    public TreplicaInvalidFieldRuntimeException() {
        super();
    }

    /**
     * Constructs a new <code>TreplicaInvalidFieldRuntimeException</code>
     * object with the specified detail message. <p>
     *
     * @param message the detail message.
     */
    public TreplicaInvalidFieldRuntimeException(String message) {
        super(message);
    }

    /**
     * Constructs a new <code>TreplicaInvalidFieldRuntimeException</code>
     * object with the specified detail message and cause. <p>
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public TreplicaInvalidFieldRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new <code>TreplicaInvalidFieldRuntimeException</code>
     * object with the specified cause and a detail message of
     * <code>cause.toString()</code>. <p>
     *
     * @param cause the cause.
     */
    public TreplicaInvalidFieldRuntimeException(Throwable cause) {
        super(cause);
    }
}
