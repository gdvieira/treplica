/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2011-2022 Gustavo Maciel Dias Vieira
 * Copyright © 2011 Vinícius Lopes da Silva Rodrigo Barbieri
 * Copyright © 2013 Henrique Squinello
 * Copyright © 2014 Rodrigo Barbieri
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.common;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.unicamp.treplica.TreplicaIOException;
import br.unicamp.treplica.TreplicaSerializationException;

/**
 * This class implements a UDP multicast transport. It allows to send and
 * receive multicast and unicast messages. Unicast addressing is done using
 * the transport id object returned by <code>getId()</code>. <p>
 * 
 * This class needs a functional UDP multicast route to work. Multicast routing
 * is no different than unicast routing: packets are routed accordingly to the
 * routes netmasks and the multicast IP. As it is very uncommon to have explicit
 * multicast routes, multicast packets will be usually routed to the default
 * gateway subnet. For hosts with only one interface, it is necessary that this
 * subnet delivers multicast packets correctly (not firewalled). If the host has
 * two or more interfaces and the default gateway subnet isn't suitable for
 * multicast, an explicit route for multicast traffic must be created pointing
 * to a suitable subnet. For example, in Linux a route could be set with this
 * command: <p>
 * <code>   route add -net 224.0.0.0/4 eth0</code> <p>
 * 
 * Also, this class needs at least one interface in the UP state or else it
 * can't determine the address to reply to multicast packets. This is true even
 * if the multicast packets are routed to the lo interface. The UP interface
 * doesn't need to have any type of connectivity and can be firewalled. <p>
 * 
 * FIXME: Receive queue is unbounded. Establish a maximum size for the receive
 *        queue. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class UDPTransport implements Transport {

    protected static final int MAX_DATA_SIZE = 65507; /* 20 bytes IP, 8 bytes UDP */
    protected static final int HEADER_SIZE = 6; 
    private static final String DEFAULT_MULT_IP = "225.0.0.0";
    private static final int DEFAULT_MULT_PORT = 6666;

    protected Logger logger;
    private InetAddress mIP;
    private int mPort;
    private DatagramSocket uSocket;
    private MulticastSocket mSocket;
    private UDPTransportId id;
    protected BlockingQueue<UDPMessage> messageQueue;

    /**
     * Creates a new UDP transport with a default multicast IP and port. <p>
     * 
     * @throws TreplicaIOException if an I/O error occurs.
     */
    public UDPTransport() throws TreplicaIOException {
        this(DEFAULT_MULT_IP, DEFAULT_MULT_PORT);
    }

    /**
     * Creates a new UDP transport with provided multicast IP and port. <p>
     * 
     * @param address the multicast ip address.
     * @param port the multicast port.
     * @throws TreplicaIOException if an I/O error occurs.
     */
    public UDPTransport(String address, int port) throws TreplicaIOException {
        logger = LoggerFactory.getLogger(this.getClass());
        try {
            mIP = InetAddress.getByName(address);
            mPort = port;
            uSocket = new DatagramSocket();
            mSocket = new MulticastSocket(mPort);
            mSocket.joinGroup(mIP);

            id = discoverTransportId();
            messageQueue = new SynchronousQueue<>();
            Thread t = new Thread(new Receiver(uSocket, id),
                                  "UDPTransport Unicast Receiver Thread");
            t.setDaemon(true);
            t.start();
            t = new Thread(new Receiver(mSocket, null), 
                           "UDPTransport Multicast Receiver Thread");
            t.setDaemon(true);
            t.start();
        } catch (IOException e) {
            throw new TreplicaIOException(e);
        }
    }

    @Override
    public TransportId getId() {
        return id;
    }

    @Override
    public Message receiveMessage() {
        return receiveMessage(0);
    }

    @Override
    public Message receiveMessage(int timeout) {
        try {
            if (timeout == 0) {
                return messageQueue.take();
            }
            return messageQueue.poll(timeout, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException e) {
        }
        return null;
    }

    @Override
    public void sendMessage(Serializable message) 
            throws TreplicaIOException, TreplicaSerializationException {
        sendMessage(message, null);
    }

    @Override
    public void sendMessage(Serializable message, TransportId destination)
            throws TreplicaIOException, TreplicaSerializationException {
        InetAddress address = mIP;
        int port = mPort;
        if (destination != null) {
            UDPTransportId destId = (UDPTransportId) destination;
            try {
                address = InetAddress.getByAddress(destId.getAddress());
            } catch (UnknownHostException e) {
                throw new TreplicaIOException(e);
            }
            port = destId.getPort();
        }

        byte[][] fragments = fragment(Marshall.marshall(message));
        for (int i = 0; i < fragments.length; i++) {
            DatagramPacket packet =
                new DatagramPacket(fragments[i], fragments[i].length,
                                   address, port);
            try {
                uSocket.send(packet);
            } catch (IOException e) {
                logger.warn("I/O exception sending message.", e);
                break;
            }
        }
    }

    /**
     * Fragments a byte array in chunks that fit an UDP packet, including
     * enough information for later reassembly. <p>
     *  
     * @param source the byte array to be fragmented.
     * @return an array of byte arrays, each containing a fragment.
     */
    private byte[][] fragment(byte[] source) {
        byte fragNumber = (byte) ((source.length / (MAX_DATA_SIZE - HEADER_SIZE)) + 1);
        byte[][] fragments = new byte[fragNumber][];
        int offset = 0;
        int messageId = new Random().nextInt();
        for (byte fragment = 0; fragment < fragNumber; fragment++) {
            byte[] buffer = new byte[fragment < fragNumber - 1 ?
                    MAX_DATA_SIZE :
                    (source.length % (MAX_DATA_SIZE - HEADER_SIZE)) + HEADER_SIZE];
            ByteBuffer.wrap(buffer).putInt(messageId).put(fragment).put(fragNumber);
            for (int i = HEADER_SIZE; i < buffer.length; i++, offset++) {
                buffer[i] = source[offset];
            }
            fragments[fragment] = buffer;
        }
        return fragments;
    }

    /**
     * This class implements a message receiver for a UDP socket. It listens
     * to messages, receives them, unmarshalls the payload, and put then
     * in the receive queue. <p>
     */
    private class Receiver implements Runnable {

        private final static int CACHE_SIZE = 30;

        private DatagramSocket socket;
        private UDPTransportId receiver;
        private LRUHashMap<Integer, Fragments> fragmentCache;

        protected Receiver(DatagramSocket socket, UDPTransportId receiver) {
            this.socket = socket;
            this.receiver = receiver;
            fragmentCache = new LRUHashMap<>(CACHE_SIZE);
        }

        @Override
        public void run() {
            byte[] receiveBuffer = new byte[MAX_DATA_SIZE];
            DatagramPacket receivePacket =
                new DatagramPacket(receiveBuffer, receiveBuffer.length);
            while (true) {
                try {
                    socket.receive(receivePacket);
                    byte[] payload = reassemble(receivePacket.getData(), 
                                                receivePacket.getLength());
                    if (payload != null) {
                        UDPMessage message = new UDPMessage(
                            Marshall.unmarshall(payload),
                            new UDPTransportId(receivePacket.getAddress().getAddress(),
                                             receivePacket.getPort()), receiver);
                        try {
                            messageQueue.put(message);
                        } catch (InterruptedException e) {
                        }
                    }
                } catch (IOException e) {
                    throw new RuntimeException(
                            "Exception in UDPTransport message receiver thread. "
                            + "Thread stopped!", e);
                } catch (TreplicaSerializationException e) {
                    logger.warn("Corrupted message discarded.", e);
                }
            }
        }

        /**
         * Tries to reassemble a message using the provided fragment
         * and other fragments stored in the fragment cache. If it isn't
         * possible to reassemble the packet the fragment is cached. <p>
         *  
         * @param source the fragment to be reassembled.
         * @param length the length of the fragment.
         * @return the reassembled byte array if possible to reassemble;
         *         <code>null</code> otherwise.
         */
        private byte[] reassemble(byte[] source, int length) {
            ByteBuffer buffer = ByteBuffer.wrap(source, 0, length);
            int messageId = buffer.getInt();
            byte fragment = buffer.get();
            byte fragNumber = buffer.get();
            if (fragNumber > 1) {
                Fragments fragments = fragmentCache.get(messageId);
                if (fragments == null) {
                    fragments = new Fragments(fragNumber);
                    fragmentCache.put(messageId, fragments);
                }
                return fragments.register(fragment, copyRange(source, 0, length));
            }
            return copyRange(source, HEADER_SIZE, length);
        }

        /**
         * Creates a copy of a byte array. <p>
         * 
         * @param original the original byte array. 
         * @param from the starting position of the copy.
         * @param to the ending position of the copy.
         * @return a new byte array containing the elements between from
         *         (inclusive) and to (exclusive).
         */
        private byte[] copyRange(byte[] original, int from, int to) {
            byte[] copy = new byte[to - from];
            for (int i = 0; i < copy.length; i++) {
                copy[i] = original[i + from];
            }
            return copy;
        }

    }

    /**
     * This class stores the cached fragments of a message. <p>
     */
    private static class Fragments {
        private byte number;
        private int size;
        private byte[][] fragments;

        public Fragments(byte total) {
            number = 0;
            size = 0;
            fragments = new byte[total][];
            for (int i = 0; i < fragments.length; i++) {
                fragments[i] = null;
            }
        }

        /**
         * Registers a fragment. <p>
         * 
         * @param place the place of the fragment in the sequence.
         * @param fragment the fragment.
         * @return the reassembled message if it is complete;
         *          <code>null</code> otherwise.
         */
        public byte[] register(byte place, byte[] fragment) {
            if (fragments[place] == null) {
                fragments[place] = fragment;
                number++;
                size += fragment.length - HEADER_SIZE;
            }
            if (number >= fragments.length) {
                byte[] buffer = new byte[size];
                int position = 0;
                for (int i = 0; i < fragments.length; i++) {
                    for (int j = HEADER_SIZE; j < fragments[i].length; j++) {
                        buffer[position++] = fragments[i][j];
                    }
                }
                return buffer;
            }
            return null;
        }
    }

    /**
     * This method is a dirty hack to discover the default multicast interface
     * of this host. It is necessary for multihomed machines to make sure our
     * local id is the same as the sending address of our multicast messages.
     * <p>
     * 
     * @return the transport ID that will appear in sent multicast messages.
     * @throws IOException if an I/O error occurs.
     */
    private UDPTransportId discoverTransportId() throws IOException {
        int port = mPort;
        while (port == mPort) {
            port = new Random().nextInt(28000) + 2000;
        }
        MulticastSocket dummy = null;
        DatagramPacket dummyPacket = null;
        try {
            dummy = new MulticastSocket(port);
            dummy.joinGroup(mIP);
            byte[] marker = Long.toHexString(new Random().nextLong()).getBytes();
            Thread t = new Thread(new UDPSpammer(uSocket, mIP, port, marker));
            t.start();
            byte[] dummyBuffer = new byte[marker.length];
            dummyPacket = new DatagramPacket(dummyBuffer, dummyBuffer.length);
            dummy.receive(dummyPacket);
            while (!Arrays.equals(marker, dummyPacket.getData())) {
                dummy.receive(dummyPacket);
            }
            t.interrupt();
        } catch (IOException e){
            throw e;
        } finally {
            if (dummy != null)
                dummy.close();
        }

        byte[] address = dummyPacket.getAddress().getAddress();
        if (Arrays.equals(address, new byte[] {0, 0, 0, 0})) { // IPv4 only
            throw new IOException("Can't identify multicast sending address. " +
                    "Usually this indicates there are no interfaces UP.");
        }
        return new UDPTransportId(address, dummyPacket.getPort());
    }

    /**
     * This class sends UDP messages to a determined IP and port, through a
     * provided socket until it is interrupted. <p>
     */
    private class UDPSpammer implements Runnable {

        private DatagramSocket socket;
        private InetAddress destination;
        private int port;
        private byte[] marker;

        protected UDPSpammer(DatagramSocket socket, InetAddress destination,
                             int port, byte[] marker) {
            this.socket = socket;
            this.destination = destination;
            this.port = port;
            this.marker = marker;
        }

        @Override
        public void run() {
            DatagramPacket packet = 
                    new DatagramPacket(marker, marker.length, destination, port);
            while (true) {
                try {
                    socket.send(packet);
                    Thread.sleep(1000);
                } catch (IOException e) {
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }

}
