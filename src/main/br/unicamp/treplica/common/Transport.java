/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.common;

import java.io.Serializable;

import br.unicamp.treplica.TreplicaIOException;
import br.unicamp.treplica.TreplicaSerializationException;

/**
 * This interface represents a generic multicast transport. It allows to send
 * and receive multicast and unicast messages. Unicast addressing is done using
 * the transport id object returned by <code>getId()</code>. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public interface Transport {

    /**
     * Returns the unique id of this transport. This id can be used for
     * unicast addressing. <p>
     * 
     * @return the unique id of this transport.
     */
    TransportId getId();
    
    /**
     * Receives a message from this transport. This method behaves exactly
     * like <code>receiveMessage(int)</code> but blocks until a message is
     * available. <p>
     * 
     * @return the received message.
     * @throws TreplicaIOException if an I/O error occurs.
     * @throws TreplicaSerializationException if a serialization error occurs.
     * @see Transport#receiveMessage(int)
     */
    Message receiveMessage()
        throws TreplicaIOException, TreplicaSerializationException;
    
    /**
     * Receives a message from this transport. This method blocks until
     * a message is available or a specified number or milliseconds passes.
     * The received message can either be a multicast or unicast one. Unicast
     * messages have as receiver this transport id, multicast messages have
     * <code>null</code> as receiver. <p>
     * 
     * @param timeout the time in milliseconds to wait for a message, 0 to
     *                wait forever.
     * @return the received message, or <code>null</code> if timeout occurs.
     * @throws TreplicaIOException if an I/O error occurs.
     * @throws TreplicaSerializationException if a serialization error occurs.
     */
    Message receiveMessage(int timeout)
        throws TreplicaIOException, TreplicaSerializationException;
    
    /**
     * Sends a multicast message. All connected transports, including this
     * one, will receive this message. Message delivery is best effort only,
     * there is no guarantee the message will reach the intended
     * destinations. <p>
     * 
     * @param message the message to be sent.
     * @throws TreplicaIOException if an I/O error occurs.
     * @throws TreplicaSerializationException if a serialization error occurs.
     */
    void sendMessage(Serializable message)
        throws TreplicaIOException, TreplicaSerializationException;
    
    /**
     * Sends a unicast message to a transport identified by a transport id.
     * Message delivery is best effort only, there is no guarantee the
     * message will reach the intended destination. <p>
     * 
     * @param message the message to be sent.
     * @param destination the destination.
     * @throws TreplicaIOException if an I/O error occurs.
     * @throws TreplicaSerializationException if a serialization error occurs.
     */
    void sendMessage(Serializable message, TransportId destination)
        throws TreplicaIOException, TreplicaSerializationException;

}
