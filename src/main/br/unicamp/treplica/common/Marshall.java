/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import br.unicamp.treplica.TreplicaSerializationException;

/**
 * This class holds utility functions related to marshalling and unmarshalling
 * of serializable objects. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public class Marshall {

    /**
     * Marshalls a serializable object into a byte array. <p>
     * 
     * @param o the object to the marshalled.
     * @return a newly allocated byte array containing the marshalled object. 
     * @throws TreplicaSerializationException if a serialization error occurs.
     */
    public static byte[] marshall(Serializable o) throws TreplicaSerializationException {
        ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
        try {
            ObjectOutputStream out = new ObjectOutputStream(outBuffer);
            out.writeObject(o);
        } catch (IOException e) {
            throw new TreplicaSerializationException(e);
        }
        return outBuffer.toByteArray();
    }

    /**
     * Unmarshalls a serializable object from a byte array. <p>
     * 
     * @param buffer the byte array.
     * @return the unmarshalled object. 
     * @throws TreplicaSerializationException if a serialization error occurs.
     */
    public static Serializable unmarshall(byte[] buffer)
            throws TreplicaSerializationException {
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new ByteArrayInputStream(buffer, 0, buffer.length));
            return (Serializable) in.readObject();
        } catch (ClassNotFoundException e) {
            throw new TreplicaSerializationException(e);
        } catch (IOException e) {
            throw new TreplicaSerializationException(e);
        }
    }

}
