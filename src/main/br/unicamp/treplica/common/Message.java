/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.common;

import java.io.Serializable;


/**
 * This interface represents a message. It holds the payload and the 
 * transport ids of sender and receiver sockets. <p>
 *
 * @author Gustavo Maciel Dias Vieira
 */
public interface Message {
    
    /**
     * Returns the payload of this message. <p>
     * 
     * @return the payload of this message.
     */
    Serializable getPayload();
    
    /**
     * Returns the id of the socket that sent this message. <p>
     * 
     * @return the id of the socket that sent this message.
     */
    TransportId getSender();
    
    /**
     * Returns the id of the socket that received this message. <p>
     * 
     * @return the id of the socket that received this message. <p>
     */
    TransportId getReceiver();
    
}
