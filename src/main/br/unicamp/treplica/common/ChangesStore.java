/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Luiz Eduardo Buzato
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * 
 * ChangesStore is a hash that is indexed by changeBlockId. Checkpoints are
 * induced either by Treplica or the application. Changes are only induced by
 * Treplica to record the changes it makes. An example might serve better as an
 * succinct explanation on checkpoints, change blocks and changes.
 * 
 * checkpoint 0
 * change blocks 0, 1, 2, ...
 * Each change block is a sequence of Serializables.
 * 
 * checkpoint 1
 * change blocks 0, 1, 2, ...
 * and so forth
 * 
 * @author Luiz Eduardo Buzato
 */
public class ChangesStore {

    HashMap<Long, LinkedList<Serializable>> changes = new HashMap<>();

    Iterator<Long> changeBlockKeys = null;
    Long currentChangeBlockKey = null;
    LinkedList<Serializable> currentChangeBlock = null;

    int numberOfChanges = 0;

    Serializable nextChange = null;
    Iterator<Serializable> changeKeys = null;

    ChangesStore() {
        nextChange = null;
        changeBlockKeys = null;
        currentChangeBlock = null;
        currentChangeBlockKey = null;
        changes.clear();
    }

    public void writeChange(long changeBlockId, Serializable change) {
        LinkedList<Serializable> changesLog = null;

        if (changes.containsKey(changeBlockId)) {
            changesLog = changes.get(changeBlockId);
        } else {
            changesLog = new LinkedList<>();
            changes.put(changeBlockId, changesLog);
        }

        changesLog.add(change);

    }

    /**
     * Reads a block of changes from the store.
     * 
     * @param changeBlockId
     *                The identifier of the changes block
     * @return The block of changes, null if no change block with the specified
     *         changeBlockId has been found.
     * 
     */
    public LinkedList<Serializable> readChanges(long changeBlockId) {

        LinkedList<Serializable> auxChanges = null;

        if (changes.containsKey(changeBlockId)) {
            auxChanges = changes.get(changeBlockId);
        }

        return auxChanges;
    }

    public void deleteChanges(long changeBlockId)
            throws NoSuchElementException {

        if (changes.containsKey(changeBlockId)) {
            changes.remove(changeBlockId);
        } else {
            throw new NoSuchElementException();
        }

    }

    /*
     * 
     * This are the key methods for reading a changesStore.
     * 
     */

    public long resetChangeStore() {

        LinkedList<Serializable> auxChangeBlock;

        currentChangeBlock = null;
        numberOfChanges = 0;

        if (!changes.isEmpty()) {
            changeBlockKeys = (changes.keySet()).iterator();
            while (changeBlockKeys.hasNext()) {
                auxChangeBlock = changes.get(changeBlockKeys.next());
                numberOfChanges = numberOfChanges + auxChangeBlock.size();
            }

            changeBlockKeys = (changes.keySet()).iterator();
            currentChangeBlock = changes.get(changeBlockKeys.next());
            if (currentChangeBlock != null) {
                if (!currentChangeBlock.isEmpty()) {
                    changeKeys = currentChangeBlock.iterator();
                }
            }
        }

        return numberOfChanges;

    }

    public long nextChangeBlockId() {
        return changes.size();
    }

    public boolean hasNextChange() {
        boolean hasChanges = false;

        if (numberOfChanges > 0)
            hasChanges = true;

        return hasChanges;
    }

    public Serializable nextChange() throws NoSuchElementException {

        nextChange = findNextChange();
        if (nextChange == null)
            throw new NoSuchElementException();

        return nextChange;
    }

    private Serializable findNextChange() {
        Serializable theNextChange = null;

        if (changes == null || changeKeys == null || changeBlockKeys == null
                || currentChangeBlock == null) {
            return theNextChange;
        }

        if (changeKeys.hasNext()) {
            theNextChange = changeKeys.next();
        } else {
            if (changeBlockKeys.hasNext()) {
                currentChangeBlock = changes.get(changeBlockKeys.next());
                if (!currentChangeBlock.isEmpty()) {
                    changeKeys = currentChangeBlock.iterator();
                    if (changeKeys.hasNext()) {
                        theNextChange = changeKeys.next();
                    }
                }
            }
        }

        if (theNextChange != null)
            numberOfChanges--;

        return theNextChange;
    }

}
