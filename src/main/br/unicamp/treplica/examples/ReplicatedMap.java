/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010-2022 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.examples;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import br.unicamp.treplica.Action;
import br.unicamp.treplica.StateMachine;
import br.unicamp.treplica.TreplicaIOException;
import br.unicamp.treplica.TreplicaSerializationException;

/**
 * This class implements a replicated and persistent map using Treplica. <p>
 * 
 * This map implements all operations of a map, but changes can only be made
 * to the map itself and not to the keys, values or entries collections. <p>
 * 
 * FIXME: Revise and check return values and exceptions, now that Treplica
 * supports them.
 *
 * @author Gustavo Maciel Dias Vieira
 * 
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 */
public class ReplicatedMap<K, V> implements Map<K, V> {
    
    private StateMachine stateMachine;
    
    public ReplicatedMap(int nProcesses, int rtt, String stableMedia) 
            throws TreplicaIOException, TreplicaSerializationException {
        this(new HashMap<K, V>(), nProcesses, rtt, stableMedia);
    }

    public ReplicatedMap(Map<K, V> map, int nProcesses, int rtt, String stableMedia) 
            throws TreplicaIOException, TreplicaSerializationException {
        stateMachine = StateMachine.createPaxosSM((Serializable) map, rtt, 
                nProcesses, false, stableMedia);
    }

    private Map<K, V> getMap() {
        @SuppressWarnings("unchecked")
        HashMap<K, V> map = (HashMap<K, V>) stateMachine.getState();
        return map;
    }
    
    private Map<K, V> getUnmodifiableMap() {
        return Collections.unmodifiableMap(getMap());
    }
    
    @Override
    public void clear() {
        try {
            stateMachine.execute(new ClearAction<K, V>());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean containsKey(Object key) {
        return getMap().containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return getMap().containsValue(value);
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return getUnmodifiableMap().entrySet();
    }

    @Override
    public V get(Object key) {
        return getMap().get(key);
    }

    @Override
    public boolean isEmpty() {
        return getMap().isEmpty();
    }

    @Override
    public Set<K> keySet() {
        return getUnmodifiableMap().keySet();
    }

    @Override
    public V put(K key, V value) {
        try {
            stateMachine.execute(new PutAction<>(key, value));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        try {
            stateMachine.execute(new PutAllAction<K, V>(m));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public V remove(Object key) {
        try {
            stateMachine.execute(new RemoveAction<K, V>(key));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    @Override
    public int size() {
        return getMap().size();
    }

    @Override
    public Collection<V> values() {
        return getUnmodifiableMap().values();
    }

    public void takeCheckpoint()
            throws TreplicaIOException, TreplicaSerializationException {
        stateMachine.checkpoint();
    }

    protected static abstract class MapAction<K, V> 
            implements Action {
        private static final long serialVersionUID = -6869113999902100650L;

        @Override
        @SuppressWarnings("unchecked")
        public Object executeOn(Object stateMachine) {
            return executeOnMap((Map<K, V>) stateMachine);
        }
        
        public abstract Object executeOnMap(Map<K, V> map);
        
    }
    
    protected static class ClearAction<K, V> extends MapAction<K, V> {
        private static final long serialVersionUID = 2254640730942764981L;

        @Override
        public Object executeOnMap(Map<K, V> map) {
            map.clear();
            return null;
        }
        
    }
    
    protected static class PutAction<K, V> extends MapAction<K, V> {
        private static final long serialVersionUID = 5571337399619932368L;
        
        private K key;
        private V value;
        
        public PutAction(K key, V value) {
            this.key = key;
            this.value = value;
        }
        
        @Override
        public Object executeOnMap(Map<K, V> map) {
            map.put(key, value);
            return null;
        }
        
    }

    protected static class PutAllAction<K, V> extends MapAction<K, V> {
        private static final long serialVersionUID = -6483055420159223231L;
        
        private Map<? extends K, ? extends V> m;
        
        public PutAllAction(Map<? extends K, ? extends V> m) {
            this.m = m;
        }
        
        @Override
        public Object executeOnMap(Map<K, V> map) {
            map.putAll(m);
            return null;
        }
        
    }

    protected static class RemoveAction<K, V> extends MapAction<K, V> {
        private static final long serialVersionUID = 2851481535406574278L;
        
        private Object key;
        
        public RemoveAction(Object key) {
            this.key = key;
        }
        
        @Override
        public Object executeOnMap(Map<K, V> map) {
            map.remove(key);
            return null;
        }
        
    }

    public static void main(String[] args) throws Exception {

        if (args.length != 3) {
            System.out.println(
             "Usage: ReplicatedMap <nprocess> <rtt> <stablemedia>\n" +
             "       <nprocess>    - number of replicas\n" +
             "       <rtt>         - message round trip time\n" +
             "       <stablemedia> - where state is saved");
            System.exit(0);
        }

        ReplicatedMap<String, String> map =
            new ReplicatedMap<>(Integer.parseInt(args[0]),
                    Integer.parseInt(args[1]), args[2]);
        BufferedReader terminal =
            new BufferedReader(new InputStreamReader(System.in));
        System.out.println("ReplicatedMap Test Application");
        System.out.println("Put: p <key> <value>     Get: g <key>     Remove: r <key>");
        System.out.println("List: l                  Clear: c         Checkpoint: k");
        System.out.println("Quit: q");
        while (true) {
            System.out.print("> ");
            String command = terminal.readLine();
            if (command == null || command.length() == 0) {
                continue;
            }
            if ("q".equals(command)) {
                System.exit(0);
            } else if ('g' == command.charAt(0) && command.length() > 2) {
                String key = command.split(" ")[1];
                System.out.println(map.get(key));
            } else if ('p' == command.charAt(0) && command.length() > 2) {
                String key = command.split(" ")[1];
                String value = command.split(" ", 3)[2];
                map.put(key, value);
                System.out.println("Key added.");
            } else if ('r' == command.charAt(0) && command.length() > 2) {
                String key = command.split(" ")[1];
                map.remove(key);
                System.out.println("Key removed.");
            } else if ("c".equals(command)) {
                map.clear();
                System.out.println("All keys removed.");
            } else if ("k".equals(command)) {
                map.takeCheckpoint();
                System.out.println("Checkpoint taken.");
            } else if ("l".equals(command)) {
                for (Entry<String, String> entry : map.entrySet()) {
                    System.out.println(entry.getKey() + ": " + entry.getValue());
                }
            }
        }
    }
    
}
