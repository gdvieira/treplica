/*
 * Treplica - Paxos-based Replication Middleware
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 *
 * This file is part of Treplica.
 *
 * Treplica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package br.unicamp.treplica.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import br.unicamp.treplica.Action;
import br.unicamp.treplica.StateMachine;
import br.unicamp.treplica.TreplicaException;

public class BankRun implements Runnable {
    
    private String type;
    private PositiveAccount account;
    private StateMachine stateMachine;
    
    public BankRun(String type, int nProcesses, String stableMedia)
            throws TreplicaException {
        this.type = type;
        stateMachine = StateMachine.createPaxosSM(new PositiveAccount(),
                200, nProcesses, false, stableMedia);
        account = (PositiveAccount) stateMachine.getState();
    }
    
    public int getBalance() {
        return account.getBalance();
    }
    
    @Override
    public void run() {
        try {
            if ("normal".equals(type)) {
                normalBank();
            } else if ("aw".equals(type)) {
                autoWithdrawBank();
            } else if ("ad".equals(type)) {
                autoDepositBank();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private void normalBank() throws Exception {
        BufferedReader terminal =
            new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            String operation = null;
            try {
                operation = terminal.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (operation == null || operation.length() == 0) {
                continue;
            }
            if ("q".equals(operation)) {
                System.exit(0);
            } else if ('b' == operation.charAt(0)) {
                System.out.println(account.getBalance());
            } else if ('d' == operation.charAt(0) && operation.length() > 2) {
                Action a =
                    new PositiveAccount.DepositAction(
                            Integer.parseInt(operation.substring(2)));
                stateMachine.execute(a);
            } else if ('w' == operation.charAt(0) && operation.length() > 2) {
                Action a =
                    new PositiveAccount.WithdrawAction(
                            Integer.parseInt(operation.substring(2)));
                stateMachine.execute(a);
            } 
        }
    }

    private void autoWithdrawBank() throws Exception {
        int change = new Random().nextInt(100) + 1;
        System.out.println("My change is: " + change);
        while (true) {
            if (account.getBalance() > 100) { // Put while here to see sync vs async
                Action a =
                    new PositiveAccount.WithdrawAction(account.getBalance() - change);
                stateMachine.execute(a);
            }
            synchronized (account) {
                try {
                    account.wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    private void autoDepositBank() throws Exception {
        for (int i = 0; i < 200; i++) {
            Action a = new PositiveAccount.DepositAction(1000);
            stateMachine.execute(a);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }
    }

    public static void main(String[] args) throws TreplicaException {
        if (args.length != 3 || (!"normal".equals(args[0]) &&
                !"aw".equals(args[0]) && !"ad".equals(args[0]))) {
            System.out.println("Usage: BankRun normal|aw|ad <nprocess> <stable media>");
            System.out.println("Usage: <stable media> is either a directory or a IP:port.");
            System.exit(0);
        }
        new BankRun(args[0], Integer.parseInt(args[1]), args[2]).run();
    }
}
